/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { ComponentType, ReactNode } from "react";
import Modal from "./Modal";
import Button from "../buttons/Button";
import { Link, useHistory, useRouteMatch } from "react-router-dom";
import { BasePayload } from "../api/entitySlice";
import Form from "../forms/Form";
import Model from "../api/Model";
import Entity from "../api/Entity";
import { Item } from "../lists/ListItems";
import { ModalProps } from "./Modal";
import IconButton from "../buttons/IconButton";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { RootState } from "../../app/store";
import { ThunkDispatch, AnyAction } from "@reduxjs/toolkit";
import MoreButton from "../buttons/MoreButton";
import { MoreButtonDefaultItem } from "../buttons/MoreButton";
import isFunction from "lodash/isFunction";
import useModel from "../api/useModel";
import ErrorAlert from "../forms/ErrorAlert";

type ModelModalBodyProps<
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity,
  Q = undefined
> = {
  model: T;
  canSave?: boolean;
  canDelete?: boolean;
  onChange: (entity: P) => Promise<any>;
  onSubmit: () => void;
  loading?: boolean;
  submitted: boolean;
  resetState: () => void;
  bodyProps?: Q;
  originalEntity: P | null;
};

export type ModelModalBody<
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity,
  Q = undefined
> = ComponentType<ModelModalBodyProps<T, P, Q>>;

type ToDescriptor<T extends Model<Entity> = Model<Entity>> =
  | string
  | ((model: T, modelIsNew: boolean) => string);

// TODO: change prop names to match useModel and extend use model props
export type ModelModalProps<
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity,
  Q = undefined
> = ModalProps & {
  entityId?: number | string;
  payload: BasePayload;
  formId: string;
  closeTo: ToDescriptor;
  cancelTo?: ToDescriptor;
  saveTo?: ToDescriptor<T>;
  deleteTo?: ToDescriptor;
  header: ReactNode;
  ModelType: new (...args: any[]) => T;
  instantiate?: () => P;
  canSave?: boolean;
  canDelete?: boolean;
  cache?: boolean;
  upload?: (draft: P, current: P) => Promise<any>;
  save?: (draft: P, current: P) => Promise<any>;
  Body: ModelModalBody<T, P, Q>;
  actionItems?: Item[];
  constructActionItems?: (props: ModelModalBodyProps<T, P, Q>) => Item[];
  deleteConfirmationMessage?: string;
  deleteName?: string;
  fetch?: (
    state: RootState,
    dispatch: ThunkDispatch<unknown, unknown, AnyAction>
  ) => Promise<Entity | null>;
  afterSave?: (model: T) => any | Promise<any>;
  afterDelete?: (model: T) => any | Promise<any>;
  bodyProps?: Q;
  CustomButtons?: ModelModalBody<T, P, Q>;
  refreshCollectionId?: string;
};

const ModelModal = <
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity,
  Q = undefined
>({
  fetch,
  entityId,
  payload,
  formId,
  closeTo,
  cancelTo = closeTo,
  saveTo,
  deleteTo,
  ModelType,
  instantiate,
  canSave,
  canDelete,
  Body,
  actionItems,
  constructActionItems,
  cache,
  afterSave,
  afterDelete = afterSave,
  deleteName,
  deleteConfirmationMessage = `Are you sure you want to delete this${
    deleteName ? ` ${deleteName}` : ""
  }?`,
  upload,
  save,
  bodyProps,
  CustomButtons,
  refreshCollectionId = "true",
  closeDisabled,
  // Loading here implies don't fetch model until after this is false, but do show the modal and loading spinner
  loading,
  ...rest
}: ModelModalProps<T, P, Q>) => {
  const { url } = useRouteMatch();
  const history = useHistory();

  const {
    model,
    loading: entityLoading,
    current: entity,
    // draft,
    failed,
    errors,
    onChange,
    onSubmit,
    initializing,
    submitted,
    resetState,
    onDelete,
  } = useModel<T, P>({
    idOrSlug: entityId,
    payload,
    ModelType,
    cache,
    instantiate,
    canSave,
    canDelete,
    upload,
    save,
    fetch,
    loading,
    afterSave: async () => {
      const convertedUrl = convertDescriptor(saveTo || closeTo);
      if (convertedUrl !== url) {
        // TODO: this refresh solution is quick and dirty, should be done on each page individually
        close(`${convertedUrl}?refresh=${refreshCollectionId}`);
      }

      await afterSave?.(model);
    },
    afterDelete: async () => {
      const convertedUrl = convertDescriptor(deleteTo || closeTo);
      if (convertedUrl !== url) {
        // TODO: this refresh solution is quick and dirty, should be done on each page individually
        close(`${convertedUrl}?refresh=${refreshCollectionId}`);
      }

      await afterDelete?.(model);
    },
  });

  const modelIsNew = !model.id;

  const convertDescriptor = (to: ToDescriptor<T>) =>
    isFunction(to) ? to(model, modelIsNew) : to;

  const close = async (to: string = convertDescriptor(closeTo)) =>
    history.push(to);

  const modalBodyProps = {
    model: model,
    canSave: canSave,
    canDelete: canDelete,
    onChange: onChange,
    loading: entityLoading,
    submitted: submitted,
    resetState: resetState,
    bodyProps: bodyProps,
    onSubmit,
    originalEntity: entity,
  };

  const dropdownItems = [
    ...(constructActionItems?.(modalBodyProps) || []),
    ...(actionItems || []),
    ...(!!canDelete && !!entityId
      ? [
          {
            content: (
              <MoreButtonDefaultItem
                onClick={() =>
                  window.confirm(deleteConfirmationMessage) && onDelete()
                }
                disabled={entityLoading}
              >
                <span className="text-right flex-grow">
                  Delete
                  {deleteName ? ` ${deleteName}` : ""}
                </span>
                <IconButton
                  as="a"
                  icon={faTrashAlt}
                  variant="link-action"
                  size="sm"
                  disabled={entityLoading}
                />
              </MoreButtonDefaultItem>
            ),
          },
        ]
      : []),
  ];

  return (
    <Modal
      closeTo={closeTo}
      closeDisabled={entityLoading || closeDisabled} // || initializing ?
      loading={initializing || loading}
      {...rest}
      actions={
        !initializing && !!dropdownItems.length ? (
          <MoreButton
            listItemsProps={{
              id: `${formId}-actions`,
              items: dropdownItems,
            }}
          />
        ) : null
      }
    >
      <Form id={formId} className="flex flex-col flex-grow" onSubmit={onSubmit}>
        <Modal.Body className="gap-y-6">
          <Body {...modalBodyProps} />
          <ErrorAlert errors={errors} />
        </Modal.Body>
        {canSave ? (
          <Modal.Footer>
            <div className="flex-grow">
              <Link to={convertDescriptor(cancelTo)}>
                <Button variant="plain" disabled={entityLoading}>
                  Cancel
                </Button>
              </Link>
            </div>
            {failed && (
              <div className="text-red-500 font-medium">Failed to save</div>
            )}
            {CustomButtons ? (
              <CustomButtons {...modalBodyProps} />
            ) : (
              <Button type="submit" variant="primary" disabled={entityLoading}>
                {"Save"}
              </Button>
            )}
          </Modal.Footer>
        ) : null}
      </Form>
    </Modal>
  );
};

export default ModelModal;
