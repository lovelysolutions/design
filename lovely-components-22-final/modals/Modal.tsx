/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import { faTimes } from "@fortawesome/free-solid-svg-icons";
import React, { Component, HTMLProps, ReactNode } from "react";
import IconButton from "../buttons/IconButton";
import classNames from "classnames";
import { Link } from "react-router-dom";

import { LocationDescriptor } from "history";
import Spinner from "../../loading/Spinner";

export interface ModalProps {
  header?: ReactNode;
  actions?: ReactNode;
  closeTo?: LocationDescriptor;
  closeDisabled?: boolean;
  loading?: boolean;
  size?: string;
}

interface ModalOuterSectionLabelProps {
  first?: boolean;
  colorClasses?: string;
}
export const ModalOuterSectionLabel: React.FC<ModalOuterSectionLabelProps> = ({
  children,
  first,
  colorClasses,
}) => (
  <div
    className={classNames(
      "uppercase text-sm font-medium pb-1",
      first ? "pt-1" : "pt-6",
      colorClasses || "text-gray-400"
    )}
  >
    {children}
  </div>
);

export const ModalInnerSectionLabel: React.FC<HTMLProps<HTMLDivElement>> = ({
  children,
  className,
}) => (
  <div
    className={classNames(
      "uppercase text-xs text-gray-600 font-semibold",
      className
    )}
  >
    {children}
  </div>
);

const Body: React.FC<React.HTMLProps<HTMLDivElement>> = ({
  children,
  className,
}) => (
  <div className={classNames("px-7 pb-7 flex-grow flex flex-col", className)}>
    {children}
  </div>
);

const Footer: React.FC<React.HTMLProps<HTMLDivElement>> = ({
  children,
  className,
}) => (
  <div
    className={classNames(
      "px-7 py-6 w-full flex items-center gap-x-4",
      className || "justify-end"
    )}
    style={{
      // TODO: move into tailwind?
      boxShadow: "0 -4px 6px 0 rgba(0,0,0,0.04)",
    }}
  >
    {children}
  </div>
);

export default class Modal extends Component<ModalProps> {
  static Footer = Footer;
  static Body = Body;

  render() {
    const {
      children,
      header,
      closeTo,
      closeDisabled,
      actions,
      loading,
      size = "sm:max-w-3xl",
    } = this.props;

    return (
      <div
        className="relative z-100"
        aria-labelledby="modal-title"
        role="dialog"
        aria-modal="true"
      >
        <div
          className="fixed print:hidden inset-0 bg-gray-500 bg-opacity-75 transition-opacity z-10"
          aria-hidden="true"
        />
        <div className="fixed print:static z-10 inset-0 print:bottom-auto overflow-y-auto print:overflow-y-visible safe-area-inset-top safe-area-inset-bottom">
          <div className="flex items-start justify-center min-h-screen py-2 px-2 print:p-0 text-center sm:block">
            {/* <span
            className="hidden sm:inline-block sm:align-start sm:h-screen"
            aria-hidden="true"
          >
            &#8203;
          </span> */}

            <div
              className={classNames(
                "inline-block align-bottom bg-white rounded text-left overflow-visible shadow-xl transform transition-all sm:my-8 print:m-0 sm:align-middle w-full print:block print:max-w-none",
                size
              )}
            >
              <div
                className={classNames("flex flex-col gap-y-6")}
                // TODO: necessary?
                style={{ minHeight: "30rem" }}
              >
                <div className="pl-7 pr-3 pt-4 font-semibold flex items-center text-gray-600 text-lg flex-nowrap gap-x-4 h-16">
                  {!!header && (
                    <div className="flex-grow overflow-hidden overflow-ellipsis whitespace-nowrap">
                      {header}
                    </div>
                  )}
                  <div className="flex items-center gap-x-2 ml-auto">
                    {actions}
                    {!!closeTo && (
                      <Link to={closeTo}>
                        <IconButton
                          icon={faTimes}
                          variant="link-plain"
                          className="p-4"
                          disabled={closeDisabled}
                        />
                      </Link>
                    )}
                  </div>
                </div>
                {loading ? <Spinner /> : children}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
