/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { ComponentType, forwardRef, HTMLProps } from "react";
import classNames from "classnames";

type ButtonVariant =
  | "primary"
  | "plain"
  | "action"
  | "link"
  | "inverted"
  | "link-action"
  | "link-plain"
  | "link-primary"
  | "unstyled";

export type ButtonProps = Omit<
  React.HTMLProps<HTMLButtonElement | HTMLAnchorElement>,
  "size"
> & {
  type?: "button" | "submit" | "reset";
  variant?: ButtonVariant;
  as?: string | ComponentType<HTMLProps<HTMLButtonElement>>;
  stopPropagation?: boolean;
  preventDefault?: boolean;
};

export const BUTTON_VARIANT_LINK =
  "font-medium text-base text-blue-500 hover:text-blue-600";
export const BUTTON_VARIANT_LINK_ACTION =
  "font-medium text-base text-textOrange hover:opacity-80";
export const BUTTON_VARIANT_LINK_PLAIN_ACTION =
  "font-medium text-base text-gray-600 hover:text-textOrange";
export const BUTTON_VARIANT_LINK_PRIMARY =
  "font-medium text-base text-buttonOrange hover:opacity-80";
export const BUTTON_VARIANT_PRIMARY =
  "bg-buttonPrimary text-white rounded font-semibold hover:opacity-80";
export const BUTTON_VARIANT_PLAIN =
  "bg-white text-black rounded border font-semibold hover:bg-gray-50";
export const BUTTON_VARIANT_ACTION =
  "bg-buttonOrange rounded text-white font-semibold hover:opacity-80";
export const BUTTON_VARIANT_ACTION_INVERTED =
  "border border-buttonOrange text-buttonOrange rounded font-semibold bg-white hover:bg-gray-50 hover:shadow";

const VARIANT_CLASS_MAP = {
  primary: BUTTON_VARIANT_PRIMARY,
  plain: BUTTON_VARIANT_PLAIN,
  action: BUTTON_VARIANT_ACTION,
  link: BUTTON_VARIANT_LINK,
  "link-action": BUTTON_VARIANT_LINK_ACTION,
  "link-plain": BUTTON_VARIANT_LINK_PLAIN_ACTION,
  "link-primary": BUTTON_VARIANT_LINK_PRIMARY,
  inverted: BUTTON_VARIANT_ACTION_INVERTED,
  // TODO: better?
  unstyled: " ",
};

const VARIANT_CLASS_MAP_PADDING = {
  primary: "py-2 px-3",
  plain: "py-2 px-3",
  action: "py-2 px-3",
  link: undefined,
  "link-action": undefined,
  "link-plain": undefined,
  "link-primary": undefined,
  inverted: "py-2 px-3",
  unstyled: undefined,
};

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      children,
      className,
      variant = "link",
      as = "button",
      stopPropagation,
      preventDefault,
      type = "button",
      ...rest
    },
    ref
  ) => {
    const Component = as;

    let props = { ...rest };

    if (stopPropagation || preventDefault) {
      props = {
        ...props,
        onClick: (event) => {
          if (stopPropagation) {
            event?.stopPropagation?.();
          }
          if (preventDefault) {
            event?.preventDefault?.();
          }
          rest.onClick?.(event);
        },
      };
    }

    return (
      <Component
        {...props}
        className={classNames(
          "disabled:opacity-50",
          VARIANT_CLASS_MAP[variant] || BUTTON_VARIANT_LINK,
          className || VARIANT_CLASS_MAP_PADDING[variant]
        )}
        ref={ref}
        type={type}
      >
        {children}
      </Component>
    );
  }
);

Button.displayName = "Button";

export default Button;
