/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef } from "react";
import Button from "./Button";
import { ButtonProps } from "./Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp, SizeProp } from "@fortawesome/fontawesome-svg-core";
import classNames from "classnames";

type IconButtonProps = ButtonProps & {
  size?: SizeProp;
  icon: IconProp;
  inline?: boolean;
};

const SIZE_TO_TEXT_SIZE_MAP: { [key: string]: string } = {
  xs: "xs",
  sm: "xs",
};

const IconButton = forwardRef<HTMLButtonElement, IconButtonProps>(
  ({ children, icon, size, inline, ...rest }, ref) => {
    const textClasses = () => {
      return classNames(
        `text-${SIZE_TO_TEXT_SIZE_MAP[size || "md"] || "sm"}`,
        inline && "text-left"
      );
    };

    return (
      <Button {...rest} ref={ref}>
        <div
          className={
            inline
              ? "flex items-center gap-x-2"
              : "flex flex-col items-center gap-y-0.5"
          }
        >
          <FontAwesomeIcon icon={icon} size={size} />
          {children && <div className={textClasses()}>{children}</div>}
        </div>
      </Button>
    );
  }
);

IconButton.displayName = "IconButton";

export default IconButton;
