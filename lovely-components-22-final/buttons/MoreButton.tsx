/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import Dropdown from "../dropdowns/Dropdown";
import IconButton from "./IconButton";
import { UnstyledListItem } from "../lists/ListItems";
import classNames from "classnames";
import Button, { ButtonProps } from "./Button";
import { DropdownProps } from "../dropdowns/Dropdown";

type MoreButtonProps = React.HTMLProps<HTMLDivElement> &
  DropdownProps & {
    buttonClasses?: string;
    size?: string;
  };

export const MoreButtonDefaultItem: React.FC<ButtonProps> = ({
  children,
  className,
  ...rest
}) => (
  <Button
    variant="unstyled"
    className={classNames(
      "px-3.5 text-sm bg-transparent hover:bg-gray-50 flex items-center gap-x-3.5 font-semibold whitespace-nowrap",
      className || "py-3"
    )}
    {...rest}
    ref={null}
  >
    {children}
  </Button>
);

const MoreButton: React.FC<MoreButtonProps> = ({
  listItemsProps,
  className,
  buttonClasses,
  size,
  ...rest
}) => (
  <Dropdown
    listItemsProps={{
      ItemComponent: UnstyledListItem,
      ...listItemsProps,
    }}
    className={classNames("right-0", className)}
    {...rest}
  >
    <IconButton
      icon={faEllipsisV}
      variant="link-plain"
      className={classNames(buttonClasses || "p-4")}
      size={size}
      preventDefault
      stopPropagation
    />
  </Dropdown>
);

export default MoreButton;
