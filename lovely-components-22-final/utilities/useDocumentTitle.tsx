// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React, { useEffect } from "react";
import { useLocation, useRouteMatch } from "react-router-dom";

const useDocumentTitle = (title?: string) => {
  const { url } = useRouteMatch();
  const location = useLocation<any>();

  useEffect(() => {
    if (!title || location.pathname !== url) {
      return;
    }

    document.title = title;
  }, [title, location.pathname]);
};

export default useDocumentTitle;
