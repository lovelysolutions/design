/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef } from "react";
import classNames from "classnames";
import { Menu } from "@headlessui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { Filter, ListItemComponent } from "../lists/ListItems";
import ListItems from "../lists/ListItems";
import { CollectionFilterItem } from "../api/collectionSlice";
import Model from "../api/Model";
import Entity from "../api/Entity";

type DropdownFilterProps<
  T extends Entity = Entity,
  P extends Model<T> = Model<T>
> = React.HTMLProps<HTMLButtonElement> & {
  filter: Filter<T, P>;
  onFilter: (items: CollectionFilterItem[]) => void;
  activeItems?: CollectionFilterItem[];
};

interface DropdownFilterItemProps {
  onClick?: (item: CollectionFilterItem) => void;
  itemActive?: (item: CollectionFilterItem) => boolean;
}

const DropdownFilterItem: ListItemComponent = ({ item, componentProps }) => {
  if (!componentProps) {
    throw new Error("componentProps required in dropdown filter item");
  }

  if (!item) {
    return null;
  }

  const collectionFilterItem = { name: item.name || "", value: item.value };

  // TODO: fix types
  const { onClick, itemActive } =
    componentProps as unknown as DropdownFilterItemProps;

  return (
    <Menu.Item>
      {({ active }) => (
        <a
          className={classNames(
            "flex flex-col gap-y-0.5 bg-transparent cursor-pointer py-1.5 px-3",
            itemActive?.(collectionFilterItem) && "font-medium",
            itemActive?.(collectionFilterItem) &&
              !active &&
              "bg-backgroundWhite",
            active && "bg-gray-100"
          )}
          onClick={() => onClick?.(collectionFilterItem)}
        >
          <div>{item.name || item.value}</div>
          {!!item.content && (
            <div className="text-xs text-gray-500">{item.content}</div>
          )}
        </a>
      )}
    </Menu.Item>
  );
};

const DropdownFilter = forwardRef<HTMLButtonElement, DropdownFilterProps>(
  ({ children, className, filter, onFilter, activeItems, ...rest }, ref) => {
    const itemActive: (item: CollectionFilterItem) => boolean = (
      item: CollectionFilterItem
    ) =>
      !!activeItems?.includes(item) ||
      !!activeItems?.find((activeItem) => activeItem.value === item.value);

    const onClick = (item: CollectionFilterItem) => {
      if (!filter.multiple || !activeItems?.length) {
        return onFilter([item]);
      }

      const items = itemActive(item)
        ? activeItems.filter((activeItem) => activeItem !== item)
        : [...activeItems, item];

      onFilter(items);
    };

    return (
      <Menu>
        {({ open }) => (
          <>
            <Menu.Button
              {...rest}
              as="button"
              ref={ref}
              className={classNames(
                "bg-transparent disabled:opacity-50 text-sm font-medium text-gray-800 flex gap-x-2 items-center relative focus:outline-transparent",
                className || "h-10 px-1"
              )}
              type="button"
            >
              <div>{children}</div>
              <FontAwesomeIcon
                icon={open ? faCaretUp : faCaretDown}
                // icon={faCaretDown}
                className={open ? "text-textOrange" : ""}
              />
            </Menu.Button>
            <Menu.Items
              static
              className={classNames(
                "absolute bg-white rounded-sm shadow py-1 overflow-y-auto z-10 w-72",
                open ? "" : "hidden"
              )}
              style={{ maxHeight: "20rem" }}
            >
              <ListItems
                ItemComponent={DropdownFilterItem}
                {...filter.listItemsProps}
                componentProps={{
                  onClick,
                  itemActive: (item: CollectionFilterItem) =>
                    !!activeItems?.includes(item),
                }}
                className="gap-y-0 divide-y divide-gray-100"
                wrapperClasses="flex-grow flex flex-col gap-y-1"
                filterClasses="px-3 py-2"
                searchClasses="bg-backgroundOffWhite rounded"
                emptyState={
                  <div className="flex flex-col gap-y-2 p-3">
                    <div className="font-medium text-black text-center text-lg">
                      {"Zero, zip, zilch, nada."}
                    </div>
                    <div className="font-medium text-gray-400 text-center">
                      {"We couldn't find anything for this search"}
                    </div>
                  </div>
                }
                // TODO: this doesn't work
                // searchInputProps={open ? { autoFocus: true } : undefined}
                searchInputProps={{
                  ...filter.listItemsProps.searchInputProps,
                  onKeyDown: (event) => {
                    if (event.key === " ") {
                      event.stopPropagation();
                    }
                  },
                }}
              />
            </Menu.Items>
          </>
        )}
      </Menu>
    );
  }
);

DropdownFilter.displayName = "DropdownFilter";

export default DropdownFilter;
