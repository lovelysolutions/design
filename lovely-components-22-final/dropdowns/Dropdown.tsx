/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { Fragment } from "react";
import classNames from "classnames";
import { Menu } from "@headlessui/react";
import ListItems, { ListItemsProps } from "../lists/ListItems";

export type DropdownProps = React.HTMLProps<HTMLDivElement> & {
  listItemsProps: ListItemsProps;
  origin?: "top" | "bottom";
};

const Dropdown: React.FC<DropdownProps> = ({
  children,
  listItemsProps,
  className,
  origin = "top",
  style,
}) => {
  return (
    <Menu
      as="div"
      className="flex relative cursor-pointer disabled:cursor-default"
    >
      <Menu.Button as={Fragment}>{children}</Menu.Button>
      <Menu.Items
        className={classNames(
          "absolute bg-white rounded-sm shadow overflow-y-auto my-2 z-10 print:hidden",
          `${origin}-full`,
          className
        )}
        style={{ ...style, maxHeight: "20rem" }}
      >
        <ListItems className="gap-y-0 divide-y" {...listItemsProps} />
      </Menu.Items>
    </Menu>
  );
};

Dropdown.displayName = "Dropdown";

export default Dropdown;
