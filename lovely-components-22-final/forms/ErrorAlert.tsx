/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React from "react";
import Alert from "../../alerts/Alert";
import { TAG_VARIANT_REJECTED } from "../../labels/Tag";
interface ErrorAlertProps {
  errors?: string[];
}
const ErrorAlert: React.FC<ErrorAlertProps> = ({ errors }) => {
  return errors?.length ? (
    <Alert variant={TAG_VARIANT_REJECTED}>
      <div className="flex flex-col space-y-1">
        <div className="font-semibold">Errors:</div>
        {errors.map((error, index) => (
          <div key={index} className="font-medium text-sm">
            {error}
          </div>
        ))}
      </div>
    </Alert>
  ) : null;
};

export default ErrorAlert;
