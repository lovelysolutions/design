/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import classNames from "classnames";
import isString from "lodash/isString";
import isBoolean from "lodash/isBoolean";
import noop from "lodash/noop";
import omit from "lodash/omit";
import React, {
  ComponentType,
  FormEventHandler,
  forwardRef,
  HTMLProps,
  MutableRefObject,
  ReactNode,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import DateInput from "../inputs/DateInput";

import Input from "../inputs/Input";
import NativeSelect from "../inputs/NativeSelect";
import Toggle from "../inputs/Toggle";
import ReactDatePicker from "react-datepicker";
import Select, { SelectOption } from "../inputs/Select";
import { SearchableSelectFieldProps } from "../inputs/SearchableSelect";
import SearchableSelect from "../inputs/SearchableSelect";
import { ValidationError } from "class-validator";
import isArray from "lodash/isArray";
import isNull from "lodash/isNull";
import isUndefined from "lodash/isUndefined";
import isNumber from "lodash/isNumber";
import Upload from "../inputs/Upload";
import { formatFullDate, formatTimeValue } from "../../utilities/dates";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import formatISO from "date-fns/formatISO";
import AvatarUpload from "../inputs/AvatarUpload";
import isObject from "lodash/isObject";
import Avatar from "../images/Avatar";
import RichText from "../inputs/RichText";
import { DateInputProps } from "../inputs/DateInput";
import PasswordInput from "../inputs/PasswordInput";
import RadioInput from "../inputs/RadioInput";

type FieldLabelProps = React.HTMLProps<HTMLLabelElement> & {
  fieldLabel?: string | ReactNode;
  inputId?: string;
  labelClasses?: string;
  inline?: boolean;
  LabelType?: string | ComponentType<HTMLProps<HTMLLabelElement>>;
};

export const Label = forwardRef<HTMLLabelElement, FieldLabelProps>(
  (
    {
      children,
      className,
      fieldLabel,
      labelClasses,
      inline,
      LabelType = "label",
      ...rest
    },
    ref
  ) => {
    return (
      <LabelType
        className={classNames(
          className,
          inline
            ? "flex gap-x-4 items-center flex-nowrap"
            : "flex flex-col gap-y-1",
          "flex-grow"
        )}
        {...omit(rest, "name", "value", "placeholder", "disabled")}
        ref={ref}
      >
        {!!inline && children}
        {isString(fieldLabel) ? (
          <div
            className={classNames(
              labelClasses || "text-xs text-gray-600 font-semibold"
            )}
          >
            {fieldLabel}
          </div>
        ) : (
          fieldLabel
        )}
        {!inline && children}
      </LabelType>
    );
  }
);

Label.displayName = "Label";

export type FieldType =
  | "richtext"
  | "avatar"
  | "date"
  | "month"
  | "quarter"
  | "nativeSelect"
  | "toggle"
  | "text"
  | "textarea"
  | "email"
  | "tel"
  | "url"
  | "password"
  | "number"
  | "decimal"
  | "select"
  | "searchableSelect"
  | "multipleSelect"
  | "upload"
  | "checkbox"
  | "radio"
  | "multiSelect"
  | "signature"
  | "time";

export type FieldProps = Omit<
  React.HTMLProps<
    | HTMLDivElement
    | HTMLInputElement
    | HTMLTextAreaElement
    | HTMLSelectElement
    | HTMLButtonElement
    | HTMLLabelElement
    | ReactDatePicker
  >,
  "value" | "ref" | "onChange" | "type"
> & {
  type?: FieldType;
  fieldLabel?: string | ReactNode;
  inputClassName?: string;
  inputClassNameOverride?: boolean;
  editing?: boolean;
  options?: SelectOption[];
  onChange?: (value: any) => void;
  value: any;
  hidden?: boolean;
  conversion?: (value: any) => any;
  selectButton?: ReactNode;
  searchableSelectProps?: SearchableSelectFieldProps;
  validate?: () => Promise<void>;
  submitted?: boolean;
  generateInvalidMessages?: (errors: ValidationError[]) => string[];
  descriptors?: ReactNode[];
  nullable?: boolean;
  multiple?: boolean;
  labelClasses?: string;
  openToDate?: Date;
  displayFormat?: (value?: string) => string | undefined;
  inline?: boolean;
  showPasswordDescriptor?: boolean;
};

const Field = forwardRef<
  | HTMLDivElement
  | HTMLInputElement
  | HTMLTextAreaElement
  | HTMLSelectElement
  | HTMLButtonElement
  | HTMLLabelElement
  | ReactDatePicker,
  FieldProps
>(
  (
    {
      editing,
      fieldLabel,
      value,
      openToDate,
      className,
      inputClassName,
      inputClassNameOverride,
      options,
      onChange,
      hidden,
      placeholder,
      conversion,
      type = "text",
      name,
      id,
      selectButton,
      searchableSelectProps,
      validate,
      submitted,
      generateInvalidMessages,
      descriptors,
      nullable = true,
      multiple,
      labelClasses,
      displayFormat,
      inline,
      showPasswordDescriptor,
      ...rest
    },
    ref
  ) => {
    const mounted = useRef(false);

    const [invalidMessages, setInvalidMessages] = useState<string[] | null>(
      null
    );

    const fieldId = useMemo(
      () => id || `${name || type}-${String(new Date().getTime())}`,
      [id]
    );

    useEffect(() => {
      mounted.current = true;
      return () => {
        mounted.current = false;
      };
    }, []);

    useEffect(() => {
      if (!mounted.current) {
        return;
      }

      if (!submitted || !validate) {
        setInvalidMessages(null);
        return;
      }

      const determineValidity = async () => {
        try {
          await validate?.();
          if (!mounted.current) {
            return;
          }
          setInvalidMessages(null);
        } catch (errors: unknown) {
          if (!mounted.current) {
            return;
          }
          setInvalidMessages(
            generateInvalidMessages && isArray(errors)
              ? generateInvalidMessages(errors as ValidationError[])
              : [`${fieldLabel || placeholder || name} is invalid`]
          );
        }
      };

      determineValidity();
    }, [value, validate, submitted]);

    if (hidden) {
      return null;
    }

    const richText = type === "richtext";

    const avatar = type === "avatar";

    const month = type === "month";
    const quarter = type === "quarter";
    const date = type === "date" || month || quarter;

    const nativeSelect = type === "nativeSelect";
    const toggle = type === "toggle";
    const number = type === "number" || type === "decimal";
    const textarea = type === "textarea";
    const checkbox = type === "checkbox";
    const radio = type === "radio";
    const time = type === "time";
    const select = type === "select";
    const searchableSelect = type === "searchableSelect";
    const multiSelect = type === "multiSelect";
    const upload = type === "upload";
    const password = type === "password";
    // const signature = type === "signature";

    const input =
      type === "text" ||
      textarea ||
      number ||
      type === "email" ||
      type === "tel" ||
      type === "url" ||
      checkbox ||
      time;

    const inlineFinal = isBoolean(inline) ? inline : toggle;

    const placeholderFinal =
      placeholder || (isString(fieldLabel) ? fieldLabel : undefined);

    let conversionMethod = conversion;
    // Default conversion methods
    if (!conversionMethod) {
      if (toggle) {
        conversionMethod = Boolean;
      }
      if (number) {
        conversionMethod = (value: string) => {
          const converted = parseFloat(value);
          return isNaN(converted) || value[value.length - 1] === "."
            ? value
            : converted;
        };
      }
      if (time) {
        conversionMethod = (value: any) => {
          if (!isString(value)) {
            return value;
          }
          const parts = value.split(":");
          if (parts.length < 2) {
            return value;
          }
          const dateTime = setMinutes(
            setHours(new Date().setHours(0, 0, 0, 0), +parts[0]),
            +parts[1]
          );
          return formatISO(dateTime);
        };
      }
    }

    const toggleElement = toggle && (
      <Toggle
        {...rest}
        ref={ref as MutableRefObject<HTMLButtonElement>}
        value={value}
        className={classNames(
          "h-10 box-content",
          editing && "px-0.5",
          inputClassName
        )}
        borderClassName={invalidMessages?.length ? "border-red-400" : undefined}
        onChange={onChange || noop}
        name={name}
        disabled={rest.disabled || !editing}
      />
    );

    if (editing) {
      const onUpload = (files: File[]) => {
        onChange?.(multiple ? files : files[0]);
      };

      const convertOnChange = (raw: any) => {
        if (nullable && raw === "null") {
          return onChange?.(null);
        }
        if (nullable && number && isString(raw) && raw.trim() === "") {
          return onChange?.(null);
        }

        const nullOut = nullable && (isNull(raw) || isUndefined(raw));

        onChange?.(!nullOut && conversionMethod ? conversionMethod(raw) : raw);
      };

      const onMultiSelectChange = (response: any) => {
        const updatedValue = isArray(value) ? [...value] : [];
        const index = updatedValue.findIndex(
          (responseValue) => responseValue == response
        );
        if (index === -1) {
          updatedValue.push(response);
        } else {
          updatedValue.splice(index, 1);
        }
        onChange?.(updatedValue);
      };

      const onChangeWrapper: FormEventHandler<
        HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
      > = (event) => {
        const targetValue = (
          event.target as
            | HTMLInputElement
            | HTMLTextAreaElement
            | HTMLSelectElement
        ).value;

        convertOnChange(targetValue);
      };

      const noLabel = multiSelect || avatar;

      const sharedInputProps = {
        onChange: onChangeWrapper,
        placeholder: placeholderFinal,
        type: type,
        id: fieldId,
        name: name,
      };

      return (
        <div className={className}>
          <Label
            LabelType={noLabel ? "div" : undefined}
            inline={inlineFinal}
            className={classNames(!noLabel && "cursor-pointer relative")}
            fieldLabel={fieldLabel}
            htmlFor={multiSelect || toggle ? undefined : fieldId}
            labelClasses={classNames(
              invalidMessages?.length
                ? "font-semibold text-red-500 text-xs"
                : labelClasses
            )}
          >
            {richText && <RichText onChange={onChange} value={value} />}
            {avatar && (
              <AvatarUpload
                onChange={(image) => onChange?.(image)}
                value={value}
              />
            )}
            {date && (
              <DateInput
                {...(rest as DateInputProps)}
                ref={ref as MutableRefObject<ReactDatePicker>}
                value={value}
                placeholder={placeholderFinal}
                openToDate={openToDate}
                displayFormat={displayFormat}
                showMonthYearPicker={month}
                // showFullMonthYearPicker={month}
                showQuarterYearPicker={quarter}
                onChange={(value) => convertOnChange(value)}
                className={classNames(
                  "border-b px-0.5 box-content h-10 flex-grow w-full",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                id={fieldId}
              />
            )}
            {toggleElement}
            {nativeSelect && options && (
              <NativeSelect
                {...rest}
                ref={ref as MutableRefObject<HTMLSelectElement>}
                value={value}
                className={classNames(
                  // TODO: support this for every type or change functionality
                  inputClassNameOverride
                    ? ""
                    : "border-b px-0.5 box-content h-10 rounded-none",
                  !inlineFinal && "flex-grow",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                options={options}
                onChange={onChangeWrapper}
                placeholder={placeholderFinal}
                id={fieldId}
                name={name}
              />
            )}
            {multiSelect && options && (
              <div className="flex flex-col">
                {options?.map((option, optionIndex) => (
                  <Label
                    key={optionIndex}
                    className={classNames("cursor-pointer relative")}
                    fieldLabel={option.name}
                    inline
                  >
                    <Toggle
                      value={
                        isArray(value)
                          ? !!value.find(
                              (potentialOption) =>
                                potentialOption == option.value
                            )
                          : false
                      }
                      className={classNames(
                        "h-10 border-b border-transparent box-content",
                        editing && "px-0.5",
                        inputClassName
                      )}
                      borderClassName={
                        invalidMessages?.length ? "border-red-400" : undefined
                      }
                      onChange={() => {
                        onMultiSelectChange(option.value);
                      }}
                    />
                  </Label>
                ))}
              </div>
            )}
            {radio && (
              <RadioInput
                {...rest}
                {...sharedInputProps}
                value={value}
                ref={ref as MutableRefObject<HTMLInputElement>}
                className={classNames(
                  "px-0.5 box-content flex-grow",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                options={options || []}
                onChange={(value) => convertOnChange(value)}
              />
            )}
            {input && (
              <Input
                min={number ? 0 : undefined}
                {...rest}
                value={
                  isNumber(value)
                    ? value
                    : time && value
                    ? formatTimeValue(value)
                    : value || ""
                }
                ref={
                  ref as MutableRefObject<
                    HTMLInputElement | HTMLTextAreaElement
                  >
                }
                className={classNames(
                  "border-b px-0.5 box-content flex-grow",
                  !(textarea || radio || checkbox) && "h-10",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                {...sharedInputProps}
              />
            )}
            {password && (
              <PasswordInput
                {...rest}
                value={value || ""}
                ref={ref as MutableRefObject<HTMLInputElement>}
                className={classNames(
                  "border-b px-0.5 box-content flex-grow h-10",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                {...sharedInputProps}
              />
            )}
            {select && (
              <Select
                {...rest}
                onClick={undefined}
                onBlur={undefined}
                onChange={onChange || noop}
                value={value}
                ref={ref as MutableRefObject<HTMLButtonElement>}
                className={classNames(
                  "border-b px-0.5 box-content flex-grow w-full placeholder-gray-600",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                options={options || []}
              >
                {selectButton}
              </Select>
            )}
            {searchableSelect && !!searchableSelectProps && (
              <SearchableSelect
                {...rest}
                {...searchableSelectProps}
                onChange={onChange || noop}
                value={value}
                ref={ref as MutableRefObject<HTMLInputElement>}
                inputProps={{
                  placeholder: placeholderFinal,
                  id: fieldId,
                }}
                className={classNames(
                  "border-b px-0.5 box-content flex-grow w-full h-10",
                  placeholder && "placeholder-gray-600",
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
              />
            )}
            {upload && (
              <Upload
                {...rest}
                onChange={onUpload}
                value={
                  !!value && !multiple && !isString(value) ? [value] : value
                }
                options={{ multiple }}
                ref={ref as MutableRefObject<HTMLInputElement>}
                className={classNames(
                  inputClassName,
                  !!invalidMessages?.length && "border-red-500"
                )}
                id={fieldId}
              />
            )}
          </Label>
          {(!!invalidMessages?.length ||
            !!descriptors?.length ||
            showPasswordDescriptor) && (
            <div className="flex flex-col gap-y-2 py-2">
              {/* px-0.5 */}
              {/* TODO: give id and scroll to on form submit? */}
              {invalidMessages?.map((message, index) => (
                <div
                  className="text-red-500 text-sm"
                  key={`${fieldId}-error-${index}`}
                >
                  {message}
                </div>
              ))}
              {!!showPasswordDescriptor && (
                <div
                  className="text-gray-500 text-sm"
                  key={`${fieldId}-descriptor-password`}
                >
                  {
                    "Passwords require upper and lowercase characters, numbers, and special characters ($, !, @, etc.)"
                  }
                </div>
              )}
              {descriptors?.map((descriptor, index) => (
                <div
                  className="text-gray-500 text-sm"
                  key={`${fieldId}-descriptor-${index}`}
                >
                  {descriptor}
                </div>
              ))}
            </div>
          )}
        </div>
      );
    }

    return (
      <Label
        inline={inlineFinal}
        {...rest}
        disabled={rest.disabled}
        className={className}
        fieldLabel={fieldLabel}
        ref={ref as MutableRefObject<HTMLLabelElement>}
      >
        <div
          className={classNames(
            "border-b border-transparent px-0.5 leading-10",
            !toggle && "flex-grow",
            inputClassName
          )}
        >
          {date && value ? formatFullDate(value) : undefined}
          {!!options && options.find((option) => option.value == value)?.name}
          {!date && !options && !toggle && !isObject(value) && !avatar && value}
          {nativeSelect && value?.name ? value?.name : null}
          {searchableSelect && isObject(value) && placeholder}
          {toggleElement}
          {avatar && value && (
            <Avatar src={value} alt={placeholder} sizeClass="w-12 h-12" />
          )}
        </div>
      </Label>
    );
  }
);

Field.displayName = "Field";

export default Field;
