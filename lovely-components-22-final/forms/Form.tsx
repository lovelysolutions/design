/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, {
  ComponentType,
  FormEventHandler,
  forwardRef,
  Fragment,
  HTMLProps,
  ReactNode,
  useEffect,
  useRef,
  useState,
} from "react";
import Field, { FieldProps } from "./Field";
import { ValidatorOptions } from "class-validator";
import Model from "../api/Model";
import Entity from "../api/Entity";
import omit from "lodash/omit";

export type FormFieldProps = Omit<FieldProps, "value"> & {
  value?: any;
  afterField?: ReactNode;
};

/**
 * @param fields Form fields
 */
type FormProps = Omit<React.HTMLProps<HTMLFormElement>, "onChange"> & {
  id: string;
  fields?: FormFieldProps[];
  disabled?: boolean;
  editing?: boolean;
  validate?: (options?: ValidatorOptions) => Promise<void>;
  fieldsClassName?: string;
  model?: Model<Entity>;
  onChange?: (value: Entity) => void;
  submitted?: boolean;
  as?: string | ComponentType<HTMLProps<HTMLFormElement>>;
  skipValidation?: boolean;
  inputClassName?: string;
};

const Form = forwardRef<HTMLFormElement, FormProps>(
  (
    {
      id,
      onSubmit,
      children,
      fields,
      disabled,
      editing,
      validate,
      fieldsClassName,
      model,
      onChange,
      submitted = false,
      as = "form",
      skipValidation,
      noValidate = true,
      inputClassName,
      ...rest
    },
    ref
  ) => {
    const [formSubmitted, setFormSubmitted] = useState<boolean>(false);
    const mounted = useRef(false);
    useEffect(() => {
      mounted.current = true;
      return () => {
        mounted.current = false;
      };
    }, []);

    const submit: FormEventHandler<HTMLFormElement> = async (event) => {
      event.preventDefault();
      setFormSubmitted(true);

      try {
        // TODO: can we just rely on the onSubmit handlers to validate?
        if (skipValidation) {
          // Skip validation
        } else if (validate) {
          await validate();
        } else if (model) {
          await model.validate();
        }

        await onSubmit?.(event);
        if (mounted.current) {
          setFormSubmitted(false);
        }
      } catch (errors) {
        //
      }
    };

    const Component = as;

    return (
      <Component
        onSubmit={submit}
        id={id}
        {...rest}
        ref={ref}
        noValidate={noValidate}
      >
        {!!fields && (
          <div className={fieldsClassName || ""}>
            {fields.map((fieldProps, fieldIndex) => (
              <Fragment
                key={`form-${id}-field-${fieldIndex}${
                  editing ? "-editing" : ""
                }`}
              >
                <Field
                  editing={editing}
                  inputClassName={inputClassName}
                  id={`form-${id}-field-${fieldIndex}`}
                  {...omit(fieldProps, "afterField")}
                  disabled={fieldProps.disabled || disabled}
                  submitted={submitted || formSubmitted}
                  validate={
                    fieldProps.validate ||
                    (model && fieldProps.name
                      ? () =>
                          model.validateProperty(
                            fieldProps.name as keyof Entity
                          )
                      : undefined)
                  }
                  value={
                    fieldProps.value ||
                    (model && fieldProps.name
                      ? model.entity[fieldProps.name as keyof Entity]
                      : fieldProps.value)
                  }
                  onChange={
                    fieldProps.onChange ||
                    (model && fieldProps.name && onChange
                      ? (value: any) =>
                          onChange({
                            ...model.entity,
                            [fieldProps.name as keyof Entity]: value,
                          })
                      : undefined)
                  }
                />
                {!disabled &&
                  !fieldProps.disabled &&
                  !!editing &&
                  !fieldProps.hidden &&
                  fieldProps.afterField}
              </Fragment>
            ))}
          </div>
        )}
        {children}
      </Component>
    );
  }
);

Form.displayName = "Form";

export default Form;
