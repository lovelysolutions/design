import React, { ReactNode, useState } from "react";
import IconButton from "../buttons/IconButton";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import {
  formatMonthYear,
  formatDefault,
  isSameDayOrAfter,
  isSameDayOrBefore,
} from "../../utilities/dates";
import addMonths from "date-fns/addMonths";
import range from "lodash/range";
import format from "date-fns/format";
import addDays from "date-fns/addDays";
import getDate from "date-fns/getDate";
import getDay from "date-fns/getDay";
import setDate from "date-fns/setDate";
import setDay from "date-fns/setDay";
import subMonths from "date-fns/subMonths";

import lastDayOfMonth from "date-fns/lastDayOfMonth";
import classNames from "classnames";
import Spinner from "../../loading/Spinner";
import isBefore from "date-fns/isBefore";
import isAfter from "date-fns/isAfter";

interface CalendarProps {
  initialDate?: Date;
  content?: {
    [date: string]: ReactNode;
  };
  onViewDateChange?: (date: Date) => void;
  onSelect: (startDate: Date, endDate?: Date) => void;
  loading?: boolean;
  maxDate?: Date;
  minDate?: Date;
}

const colStartClasses = {
  0: "col-start-1",
  1: "col-start-2",
  2: "col-start-3",
  3: "col-start-4",
  4: "col-start-5",
  5: "col-start-6",
  6: "col-start-7",
};

const Calendar: React.FC<CalendarProps> = ({
  initialDate,
  content,
  onViewDateChange,
  loading,
  onSelect,
  maxDate,
  minDate,
}) => {
  const today = new Date();
  // TODO: use weekStartsOn?
  const sunday = setDay(new Date(), 0);
  const daysOfTheWeek = range(0, 7);
  const [viewDate, setViewDate] = useState<Date>(initialDate || today);

  const daysOfTheMonth = range(1, getDate(lastDayOfMonth(viewDate)) + 1).map(
    (date) => setDate(viewDate, date)
  );

  const onDateChange = (date: Date) => {
    setViewDate(date);
    onViewDateChange?.(date);
  };

  const startOfTheMonth = setDate(viewDate, 1);
  const endOfTheMonth = lastDayOfMonth(viewDate);

  return (
    <div className="flex flex-col gap-y-6 flex-grow">
      <div className="flex items-center gap-x-4 flex-grow">
        <div className="font-semibold text-gray-600 flex-grow uppercase">
          {formatMonthYear(viewDate.toISOString())}
        </div>
        <div className="flex gap-x-6">
          <IconButton
            icon={faArrowLeft}
            onClick={() => onDateChange(subMonths(viewDate, 1))}
            variant="link-plain"
            disabled={minDate && isSameDayOrBefore(startOfTheMonth, minDate)}
          />
          <IconButton
            icon={faArrowRight}
            onClick={() => onDateChange(addMonths(viewDate, 1))}
            variant="link-plain"
            disabled={maxDate && isSameDayOrAfter(endOfTheMonth, maxDate)}
          />
        </div>
      </div>
      {loading ? (
        <Spinner style={{ minHeight: "27.75rem" }} />
      ) : (
        <div className="grid grid-cols-7 gap-x-1 gap-y-5 md:gap-x-5 flex-grow">
          {daysOfTheWeek.map((addedDays, index) => {
            return (
              <div
                key={index}
                className="text-center font-semibold text-gray-500"
              >
                {format(addDays(sunday, addedDays), "EEEEE")}
              </div>
            );
          })}
          {daysOfTheMonth.map((date, index) => {
            const disabled =
              (!!minDate && isBefore(date, minDate)) ||
              (!!maxDate && isAfter(date, maxDate));

            return (
              <div
                key={index}
                className={classNames(
                  "flex flex-col w-full p-1.5 cursor-pointer bg-white hover:bg-gray-50 rounded relative border",
                  !index && colStartClasses[getDay(date)],
                  disabled && "opacity-50"
                )}
                style={{ minHeight: "4rem" }}
                onClick={() => !disabled && onSelect(date)}
              >
                <div
                  className={
                    "absolute top-0 right-0 text-xs font-semibold text-gray-500 p-1.5"
                  }
                >
                  {getDate(date)}
                </div>
                {!disabled && (
                  <div className="flex flex-grow items-center justify-center">
                    {content?.[formatDefault(date)]}
                  </div>
                )}
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Calendar;
