import React, { ReactNode } from "react";
import { formatDefault } from "../../utilities/dates";
import range from "lodash/range";
import format from "date-fns/format";
import differenceInCalendarDays from "date-fns/differenceInCalendarDays";
import addDays from "date-fns/addDays";

interface InlineCalendarProps {
  content?: {
    [date: string]: ReactNode;
  };
  onSelect: (date: Date) => void;
  startDate: Date;
  endDate: Date;
}

const InlineCalendar: React.FC<InlineCalendarProps> = ({
  content,
  onSelect,
  startDate,
  endDate,
}) => {
  const days = range(0, differenceInCalendarDays(endDate, startDate) + 1).map(
    (dateOffset) => addDays(startDate, dateOffset)
  );

  return (
    <div className="overflow-hidden border bg-white rounded-sm z-0">
      <div className="flex flex-nowrap overflow-x-auto items-stretch divide-x flex-grow">
        {days.map((date, index) => (
          <div
            key={index}
            className="flex flex-col gap-y-1 pt-1 pb-3 cursor-pointer bg-white hover:bg-gray-50 relative items-center flex-shrink-0 flex-grow-0 w-20"
            style={{ minHeight: "4rem" }}
            onClick={() => onSelect(date)}
          >
            <div className="text-xs font-semibold text-gray-500 p-1.5 whitespace-nowrap uppercase">
              {format(date, "LLL d")}
            </div>
            <div className="flex flex-grow items-center justify-center">
              {content?.[formatDefault(date)]}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default InlineCalendar;
