/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";

const Arrow: React.FC = () => {
  return (
    <FontAwesomeIcon
      className="flex-shrink-0"
      icon={faChevronRight}
      size="xs"
    />
  );
};

type TitleProps = React.HTMLProps<HTMLDivElement> & {
  lastTitle?: boolean;
};

const Title: React.FC<TitleProps> = ({ children, className, lastTitle }) => {
  return (
    <div
      className={classNames(
        "text-lg font-medium overflow-ellipsis whitespace-nowrap overflow-hidden text-gray-600 flex gap-x-4 items-center",
        // !lastTitle && "text-gray-500",
        lastTitle && "font-semibold",
        className
      )}
      style={lastTitle ? undefined : { maxWidth: "15rem" }}
    >
      {children}
    </div>
  );
};

const Image: React.FC = ({ children }) => {
  return <div className="h-full flex-shrink-0">{children}</div>;
};

class PageTitle extends Component {
  static Arrow = Arrow;
  static Title = Title;
  static Image = Image;

  render() {
    return (
      <div className="flex items-center px-3 h-full py-3 flex-shrink overflow-hidden">
        {this.props.children}
      </div>
    );
  }
}

export default PageTitle;
