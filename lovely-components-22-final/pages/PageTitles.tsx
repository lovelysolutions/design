/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { ReactNode, HTMLProps } from "react";
import { Link, useParams, useRouteMatch } from "react-router-dom";
import PageTitle from "./PageTitle";
import isFunction from "lodash/isFunction";
import isObject from "lodash/isObject";
import classNames from "classnames";
import { useAppSelector } from "../../app/hooks";
import { selectActiveRole, selectViewRole } from "../../roles/activeRoleSlice";
import Avatar from "../images/Avatar";
import { selectViewEmployer } from "../../employers/employerSlice";
import { selectDashboardLoading } from "../../dashboard/dashboardSlice";
import isString from "lodash/isString";
import useDocumentTitle from "../utilities/useDocumentTitle";

export interface Title {
  title: string | ReactNode;
  to?: string;
  total?: number;
}

type PageTitlesProps = HTMLProps<HTMLDivElement> & {
  titles: (undefined | Title | ((params: any) => Title | undefined))[];
  includeRole?: boolean;
};

const PageTitles: React.FC<PageTitlesProps> = ({ titles, includeRole }) => {
  const params = useParams() as any;
  const { url } = useRouteMatch();
  const viewRole = useAppSelector(selectViewRole);
  const activeRole = useAppSelector(selectActiveRole);
  const employer = useAppSelector(selectViewEmployer);
  const schools = !!url.match("schools");
  const partnershipType = schools ? "schools" : "employers";

  const dashboardLoading = useAppSelector(selectDashboardLoading);

  const loading = dashboardLoading;

  const generatedTitles: Title[] = titles
    .map((titleGenerator) =>
      isFunction(titleGenerator) ? titleGenerator(params) : titleGenerator
    )
    .filter(
      (potentialTitle) => isObject(potentialTitle) && potentialTitle.title
    ) as Title[];

  const lastTitle = generatedTitles[generatedTitles.length - 1]?.title;

  useDocumentTitle(isString(lastTitle) ? lastTitle : "");

  return (
    <>
      <div className="flex flex-shrink overflow-hidden items-center h-full flex-grow gap-x-3 select-none">
        {!loading && !!employer && !!activeRole && !!includeRole && (
          // TODO: make this a component
          <>
            <Link
              to={`/apprenticeship/${activeRole?.id}/${partnershipType}/${employer.id}`}
              className={
                "overflow-ellipsis overflow-hidden flex-shrink flex items-center gap-x-4"
              }
              style={{ minWidth: "5rem" }}
            >
              <Avatar
                src={employer.partner?.avatar?.defaultPhoto}
                alt={employer.partner?.name}
                sizeClass={"w-9 h-9"}
                backgroundClassName="bg-textBlueGrayAgainstBlue"
              />
              <PageTitle.Title className={"flex-shrink text-xs"}>
                {employer.partner?.name}
              </PageTitle.Title>
            </Link>
            <PageTitle.Arrow />
          </>
        )}
        {!loading && !!viewRole && !!includeRole && (
          <>
            <Link
              to={`/apprenticeship/${viewRole.id}`}
              className={
                "overflow-ellipsis overflow-hidden flex-shrink flex items-center gap-x-4"
              }
              style={{ minWidth: "5rem" }}
            >
              <Avatar
                src={viewRole.user?.avatar?.defaultPhoto}
                alt={viewRole.user?.name}
                sizeClass={"w-9 h-9"}
                backgroundClassName="bg-textBlueGrayAgainstBlue"
              />
              <PageTitle.Title className={"flex-shrink text-xs"}>
                {viewRole.user?.name}
              </PageTitle.Title>
            </Link>
            <PageTitle.Arrow />
          </>
        )}
        {generatedTitles.map((title, titleIndex) => {
          const lastTitle = titleIndex === generatedTitles.length - 1;
          const pageTitle = (
            <PageTitle.Title
              className={lastTitle ? undefined : "flex-shrink"}
              lastTitle={lastTitle}
            >
              {title.title}
              {!!title.total && (
                <span className="text-gray-400 rounded-full border py-1 px-1.5 text-xs">
                  {title.total.toLocaleString()}
                </span>
              )}
            </PageTitle.Title>
          );

          return (
            <div
              key={`page-title-${titleIndex}`}
              className={classNames(
                "flex items-center max-w-full gap-x-3",
                lastTitle ? undefined : "flex-shrink overflow-hidden"
              )}
              style={{ minWidth: "4rem" }}
            >
              {title.to ? (
                <Link
                  to={title.to}
                  className={classNames(
                    "overflow-ellipsis overflow-hidden flex",
                    lastTitle ? undefined : "flex-shrink"
                  )}
                >
                  {pageTitle}
                </Link>
              ) : (
                <div
                  className={classNames(
                    "overflow-ellipsis overflow-hidden",
                    lastTitle ? undefined : "flex-shrink"
                  )}
                >
                  {pageTitle}
                </div>
              )}
              {!lastTitle && <PageTitle.Arrow />}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default PageTitles;
