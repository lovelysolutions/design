import React from "react";
import PageTitles from "./PageTitles";
import { useAppSelector } from "../../app/hooks";
import { selectCollection } from "../api/collectionSlice";

interface CollectionPageTitlesProps {
  title: string;
  collectionId: string;
}
const CollectionPageTitles: React.FC<CollectionPageTitlesProps> = ({
  title,
  collectionId,
}) => {
  const collection = useAppSelector(selectCollection({ id: collectionId }));

  return (
    <PageTitles
      includeRole
      titles={[{ title: title, total: collection?.total }]}
    />
  );
};

export default CollectionPageTitles;
