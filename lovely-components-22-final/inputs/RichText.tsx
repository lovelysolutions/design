/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./RichText.css";

import { ContentState, convertToRaw, EditorState } from "draft-js";
import htmlToDraft from "html-to-draftjs";
import React, { forwardRef, Fragment } from "react";
import { Editor, EditorProps } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";

type RichTextProps = Omit<EditorProps, "onChange"> & {
  value?: string;
  onChange?: (value: string) => void;
};
const RichText = forwardRef<Editor, RichTextProps>(
  ({ value, onChange, ...rest }, ref) => {
    // const html = "<p>Hey this <strong>editor</strong> rocks 😀</p>";
    let editorState;
    if (value) {
      const contentBlock = htmlToDraft(value);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        editorState = EditorState.createWithContent(contentState);
      }
    }

    return (
      <>
        <Editor
          toolbar={{
            options: ["inline", "link", "list"],
            inline: {
              options: ["bold", "italic", "underline"],
            },
            list: {
              options: ["unordered", "ordered"], // "indent", "outdent"],
            },
          }}
          editorClassName="bg-white rounded px-2 pb-2 border"
          toolbarClassName="bg-white rounded border"
          editorStyle={{
            minHeight: "10rem",
          }}
          {...rest}
          ref={ref}
          defaultEditorState={editorState}
          onEditorStateChange={(updatedState) => {
            const contentState = updatedState.getCurrentContent();
            if (!contentState.hasText()) {
              return onChange?.("");
            }
            onChange?.(draftToHtml(convertToRaw(contentState)));
          }}
          // wrapperStyle={<wrapperStyleObject/>}
          // toolbarStyle={<toolbarStyleObject></toolbarStyleObject>}
        />
        {/* <textarea value={value} /> */}
      </>
    );
  }
);

RichText.displayName = "RichText";

export default RichText;
