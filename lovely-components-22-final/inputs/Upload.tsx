/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import classNames from "classnames";
import React, { HTMLProps, useCallback } from "react";
import { DropzoneOptions, useDropzone } from "react-dropzone";
import isString from "lodash/isString";

type UploadProps = Omit<HTMLProps<HTMLInputElement>, "onChange"> & {
  onChange: (files: File[]) => void;
  options?: DropzoneOptions;
  value?: string | File[];
};

const Upload: React.FC<UploadProps> = ({
  onChange,
  options,
  className,
  value,
  accept,
  children,
  // id,
  ...rest
}) => {
  const onDrop = useCallback((acceptedFiles: File[]) => {
    onChange(acceptedFiles);
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    ...options,
    onDrop,
  });

  const stripFileNameFromUrl = (url: string) => {
    const parts = url.split("/");
    const lastPart = parts[parts.length - 1];
    return lastPart.split("?")[0];
  };

  const existingFileNames = !value
    ? null
    : isString(value)
    ? stripFileNameFromUrl(value)
    : value.map((file) => file.name).join(", ");

  return (
    <div
      {...(getRootProps() as any)}
      className={classNames(
        "flex text-left border bg-gray-50 p-4 cursor-pointer hover:bg-gray-100 hover:shadow-inner font-medium rounded-sm disabled:cursor-not-allowed overflow-hidden",
        className || "",
        isDragActive && "shadow-inner"
      )}
      {...rest}
    >
      <input
        // id={id}
        {...getInputProps()}
        accept={accept}
      />
      {isDragActive ? (
        <div>Drop files here...</div>
      ) : children ? (
        children
      ) : existingFileNames ? (
        <div className="flex-grow overflow-hidden overflow-ellipsis whitespace-nowrap">
          {existingFileNames}
        </div>
      ) : (
        <div>Drop files here, or click to select files</div>
      )}
    </div>
  );
};

export default Upload;
