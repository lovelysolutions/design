/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import isEqual from "date-fns/isEqual";
import React, { forwardRef, LegacyRef } from "react";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import Input from "./Input";
import ReactDatePicker from "react-datepicker";
import NativeSelect from "./NativeSelect";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretLeft, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import Button from "../buttons/Button";
import {
  parseDate,
  formatFullDate,
  formatDefault,
} from "../../utilities/dates";
import {
  YEAR_FILTERS_DATE_PICKER,
  MONTH_FILTERS_ZERO_INDEXED,
} from "../../utilities/dates";

export type DateInputProps = Omit<ReactDatePickerProps, "onChange"> & {
  onChange?: (value: string | null | undefined) => void;
  displayFormat?: (value?: string) => string | undefined;
  placeholder?: string;
};

const DateInput = forwardRef<ReactDatePicker, DateInputProps>(
  (
    {
      value,
      onChange,
      displayFormat = formatFullDate,
      showMonthYearPicker,
      showQuarterYearPicker,
      placeholder,
      ...rest
    },
    ref
  ) => {
    const onChangeWrapper = (
      date: Date | null,
      e?: React.SyntheticEvent<any> | undefined
    ) => {
      e?.preventDefault?.();
      onChange?.(date ? formatDefault(date) : date);
    };

    const parsed = parseDate(value);

    return (
      <DatePicker
        {...rest}
        ref={ref as LegacyRef<ReactDatePicker>}
        onChange={onChangeWrapper}
        selected={parsed}
        value={displayFormat(value)}
        showMonthYearPicker={showMonthYearPicker}
        showQuarterYearPicker={showQuarterYearPicker}
        placeholderText={placeholder}
        customInput={<Input autoComplete="off" />}
        // https://github.com/Hacker0x01/react-datepicker/issues/2456#issuecomment-716547497
        // https://github.com/Hacker0x01/react-datepicker/issues/2930
        dayClassName={(date) =>
          parsed && isEqual(parsed, date)
            ? "bg-blue-500 hover:bg-blue-600"
            : null
        }
        shouldCloseOnSelect
        renderCustomHeader={({
          date,
          changeYear,
          changeMonth,
          decreaseMonth,
          increaseMonth,
          prevMonthButtonDisabled,
          nextMonthButtonDisabled,
        }) => (
          <div className="flex flex-nowrap items-center">
            <Button
              onClick={decreaseMonth}
              disabled={prevMonthButtonDisabled}
              variant="link-plain"
              className="px-3 py-2"
              preventDefault
            >
              <FontAwesomeIcon icon={faCaretLeft} />
            </Button>
            <div className="ml-auto">
              {!(showMonthYearPicker || showQuarterYearPicker) && (
                <NativeSelect
                  options={MONTH_FILTERS_ZERO_INDEXED}
                  value={date.getMonth()}
                  onChange={(event) =>
                    changeMonth(+(event.target as HTMLSelectElement)?.value)
                  }
                  className="mr-1 rounded px-1 py-0.5 border bg-white"
                />
              )}
              <NativeSelect
                options={YEAR_FILTERS_DATE_PICKER}
                value={date.getFullYear()}
                onChange={(event) =>
                  changeYear(+(event.target as HTMLSelectElement)?.value)
                }
                className="mr-1 rounded px-1 py-0.5 border bg-white"
              />
            </div>

            <Button
              onClick={increaseMonth}
              disabled={nextMonthButtonDisabled}
              variant="link-plain"
              className="px-3 py-2 ml-auto"
              preventDefault
            >
              <FontAwesomeIcon icon={faCaretRight} />
            </Button>
          </div>
        )}
      />
    );
  }
);

DateInput.displayName = "DateInput";

export default DateInput;
