/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import classNames from "classnames";
import React, { forwardRef } from "react";
import { SelectOption } from "./Select";

type NativeSelectProps = React.HTMLProps<HTMLSelectElement> & {
  options: SelectOption[];
};

const NativeSelect = forwardRef<HTMLSelectElement, NativeSelectProps>(
  (
    { className, options, name, value, multiple, placeholder, ...rest },
    ref
  ) => {
    const notSelected = !options.find(
      (optionConfig) => String(optionConfig.value) === String(value)
    );

    return (
      <select
        {...rest}
        className={classNames(
          "cursor-pointer bg-transparent disabled:bg-transparent disabled:opacity-60 disabled:cursor-not-allowed focus:outline-none focus:ring-2 focus:ring-offset-white ring-offset-1 focus:ring-buttonOrangeBackground appearance-none",
          className || "rounded px-1 py-0.5 border",
          notSelected && "opacity-60"
        )}
        placeholder={placeholder}
        ref={ref}
        value={String(value)}
        defaultValue={undefined}
        multiple={multiple}
      >
        {notSelected && (
          <option value="" hidden>
            {placeholder}
          </option>
        )}
        {options.map((optionConfig, index) => (
          <option
            key={`${name}-option-${index}`}
            disabled={optionConfig.disabled}
            value={String(optionConfig.value)}
          >
            {optionConfig.name}
          </option>
        ))}
      </select>
    );
  }
);

NativeSelect.displayName = "NativeSelect";

export default NativeSelect;
