/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import "./AvatarUpload.css";
import "react-image-crop/dist/ReactCrop.css";

import React, { HTMLProps, useEffect, useRef, useState } from "react";
import ReactCrop, {
  centerCrop,
  makeAspectCrop,
  PercentCrop,
  PixelCrop,
  ReactCropProps,
} from "react-image-crop";
import { Crop } from "react-image-crop";
import ImageComponent from "../images/Image";
import Upload from "./Upload";
import loadImage from "blueimp-load-image";
import { Avatar } from "../../avatars/Avatar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import Spinner from "../../loading/Spinner";
import isNull from "lodash/isNull";
import Button from "../buttons/Button";
import classNames from "classnames";

const TO_RADIANS = Math.PI / 180;

function toBlob(canvas: HTMLCanvasElement): Promise<Blob | null> {
  return new Promise((resolve: (blob: Blob | null) => void) => {
    canvas.toBlob(resolve);
  });
}

let previewUrl = "";

async function imageBlob(
  image: HTMLImageElement,
  crop: PixelCrop,
  scale = 1,
  rotate = 0
) {
  const canvas = document.createElement("canvas");
  canvasPreview(image, canvas, crop, scale, rotate);
  const blob = await toBlob(canvas);
  if (!blob) {
    return;
  }
  if (previewUrl) {
    URL.revokeObjectURL(previewUrl);
  }
  previewUrl = URL.createObjectURL(blob);
  return previewUrl;
}

async function imageURL(
  image: HTMLImageElement,
  crop: PixelCrop,
  scale = 1,
  rotate = 0
) {
  const canvas = document.createElement("canvas");
  canvasPreview(image, canvas, crop, scale, rotate);
  return canvas.toDataURL();
}

async function canvasPreview(
  image: HTMLImageElement,
  canvas: HTMLCanvasElement,
  crop: PixelCrop,
  scale = 1,
  rotate = 0
) {
  const ctx = canvas.getContext("2d");

  if (!ctx) {
    throw new Error("No 2d context");
  }

  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;
  // devicePixelRatio slightly increases sharpness on retina devices
  // at the expense of slightly slower render times and needing to
  // size the image back down if you want to download/upload and be
  // true to the images natural size.
  const pixelRatio = window.devicePixelRatio;
  // const pixelRatio = 1

  canvas.width = Math.floor(crop.width * scaleX * pixelRatio);
  canvas.height = Math.floor(crop.height * scaleY * pixelRatio);

  ctx.scale(pixelRatio, pixelRatio);
  ctx.imageSmoothingQuality = "high";

  const cropX = crop.x * scaleX;
  const cropY = crop.y * scaleY;

  const rotateRads = rotate * TO_RADIANS;
  const centerX = image.naturalWidth / 2;
  const centerY = image.naturalHeight / 2;

  ctx.save();

  // 5) Move the crop origin to the canvas origin (0,0)
  ctx.translate(-cropX, -cropY);
  // 4) Move the origin to the center of the original position
  ctx.translate(centerX, centerY);
  // 3) Rotate around the origin
  ctx.rotate(rotateRads);
  // 2) Scale the image
  ctx.scale(scale, scale);
  // 1) Move the center of the image to the origin (0,0)
  ctx.translate(-centerX, -centerY);
  ctx.drawImage(
    image,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight
  );

  ctx.restore();
}

type UploadProps = Omit<HTMLProps<HTMLInputElement>, "onChange"> & {
  onChange: (image: Avatar | undefined) => void;
  value?: string;
  cropProps?: ReactCropProps;
};

const DEFAULT_ASPECT_RATIO = 1;

const AvatarUpload: React.FC<UploadProps> = ({
  onChange,
  value,
  cropProps = { aspect: DEFAULT_ASPECT_RATIO },
  disabled,
  ...rest
}) => {
  const [crop, setCrop] = useState<Crop>({
    unit: "%",
    x: 0,
    y: 0,
    width: 100,
    height: 100,
  });
  const [avatar, setAvatar] = useState<Avatar | null | undefined>(null);
  const [naturalHeight, setNaturalHeight] = useState<number>(0);
  const [naturalWidth, setNaturalWidth] = useState<number>(0);
  const [image, setImage] = useState<string>();
  const [compiling, setCompiling] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const imageRef = useRef<HTMLImageElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);

  const mapCrop: (pixelCrop: PixelCrop) => Partial<Avatar> = (pixelCrop) => {
    return {
      cropH: pixelCrop.height,
      cropW: pixelCrop.width,
      cropX: pixelCrop.x,
      cropY: pixelCrop.y,
    };
  };

  const convertCrop: (
    imageCrop: PixelCrop | PercentCrop,
    width?: number,
    height?: number
  ) => PixelCrop = (
    imageCrop,
    width: number = naturalWidth,
    height: number = naturalHeight
  ) => {
    return imageCrop.unit === "px"
      ? imageCrop
      : {
          unit: "px",
          width: (imageCrop.width / 100) * width,
          height: (imageCrop.height / 100) * height,
          x: (imageCrop.x / 100) * width,
          y: (imageCrop.y / 100) * height,
        };
  };

  const onChangeWrapper = async (files: File[]) => {
    if (!files.length || !wrapperRef.current) {
      return;
    }

    setLoading(true);

    const updatedAvatar = {
      originalFilename: files[0].name,
      contentType: files[0].type,
    };
    setAvatar(updatedAvatar);

    try {
      const data = await loadImage(files[0], { orientation: true });
      if (data.image instanceof HTMLImageElement) {
        let renderedImage = await imageBlob(
          data.image,
          {
            unit: "px",
            x: 0,
            y: 0,
            width: data.image.naturalWidth,
            height: data.image.naturalHeight,
          },
          1,
          0
        );

        if (!renderedImage) {
          return;
        }

        const imageCrop = centerCrop(
          makeAspectCrop(
            { unit: "%", width: 100, height: 100 },
            cropProps.aspect || DEFAULT_ASPECT_RATIO,
            data.image.naturalWidth,
            data.image.naturalHeight
          ),
          data.image.naturalWidth,
          data.image.naturalHeight
        );

        setNaturalWidth(data.image.naturalWidth);
        setNaturalHeight(data.image.naturalHeight);

        setImage(renderedImage);
        setCrop(imageCrop);

        const pixelCrop = convertCrop(
          imageCrop,
          data.image.naturalWidth,
          data.image.naturalHeight
        );

        renderedImage = await imageURL(data.image, pixelCrop, 1, 0);

        setAvatar({
          ...updatedAvatar,
          photoData: renderedImage.split(",")[1],
          // TODO: uncomment ...mapCrop(pixelCrop),
        });
      }
    } catch (error) {
      setAvatar(undefined);
      setImage(undefined);
    }
    setLoading(false);
  };

  const onCrop = (updatedCrop: Crop, percentageCrop: PercentCrop) => {
    if (!updatedCrop.width || !updatedCrop.height) {
      return;
    }
    setCrop(percentageCrop);
  };

  const onCropComplete = async (
    updatedCrop: PixelCrop,
    percentageCrop: PercentCrop
  ) => {
    if (!updatedCrop.width || !updatedCrop.height) {
      return;
    }

    setAvatar({
      ...avatar,
      ...mapCrop(convertCrop(percentageCrop)),
    } as Avatar);

    // TODO: this code below should not be necessary - figure out what backend expects, preserve original size?
    if (!imageRef.current) {
      return;
    }

    setCompiling(true);

    setImmediate(async () => {
      if (!imageRef.current) {
        return;
      }

      const renderedImage = await imageURL(imageRef.current, updatedCrop, 1, 0);
      setAvatar({
        ...avatar,
        photoData: renderedImage.split(",")[1],
        cropH: undefined,
        cropW: undefined,
        cropX: undefined,
        cropY: undefined,
      });

      setCompiling(false);
    });
  };

  useEffect(() => {
    if (isNull(avatar)) {
      return;
    }
    onChange?.(avatar);
  }, [avatar]);

  return (
    <div ref={wrapperRef} className="flex-grow flex">
      {loading ? (
        <Spinner />
      ) : image ? (
        <div
          className={classNames(
            "flex-grow border bg-gray-50 p-4 rounded-sm flex flex-col gap-y-4 relative disabled:cursor-not-allowed",
            (compiling || disabled) && "opacity-80 cursor-not-allowed"
          )}
        >
          <div className="overflow-hidden w-full">
            <ReactCrop
              {...cropProps}
              onChange={onCrop}
              crop={crop}
              onComplete={onCropComplete}
              disabled={disabled || compiling}
              locked={disabled || compiling}
            >
              <img ref={imageRef} src={image} />
            </ReactCrop>
          </div>
          <Button
            onClick={() => {
              setAvatar(undefined);
              setImage(undefined);
            }}
          >
            Cancel
          </Button>
          {compiling && (
            <div className="absolute flex items-center justify-center inset-0 pointer-events-none">
              <Spinner />
            </div>
          )}
        </div>
      ) : (
        <div
          className={classNames(
            "relative avatar-upload",
            !value && "flex-grow"
          )}
        >
          <Upload
            {...rest}
            accept=".jpg,.jpeg,.png,.gif"
            value={value}
            onChange={onChangeWrapper}
            disabled={disabled}
          >
            {value ? (
              <div className="w-36 h-36">
                <ImageComponent src={value} />
              </div>
            ) : undefined}
          </Upload>
          {!!value && (
            <div className="absolute items-center justify-center text-white inset-0 hidden pointer-events-none avatar-upload__icon text-2xl opacity-90">
              <FontAwesomeIcon icon={faPencilAlt} />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default AvatarUpload;
