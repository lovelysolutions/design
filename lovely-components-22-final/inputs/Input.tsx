/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import classNames from "classnames";
import React, {
  FormEventHandler,
  forwardRef,
  InputHTMLAttributes,
  RefObject,
  useEffect,
  useRef,
  useState,
} from "react";

import "./input.css";
import isNumber from "lodash/isNumber";

export type InputProps = React.HTMLProps<
  HTMLInputElement | HTMLTextAreaElement
> & {
  ringClasses?: string;
};

const TYPE_INPUT_MODE_MAP: {
  [type: string]:
    | "none"
    | "text"
    | "tel"
    | "url"
    | "email"
    | "numeric"
    | "decimal"
    | "search"
    | undefined;
} = {
  text: "text",
  textarea: "text",
  number: "numeric",
  tel: "tel",
  email: "email",
  decimal: "decimal",
  search: "search",
  url: "url",
};

const Input = forwardRef<HTMLInputElement | HTMLTextAreaElement, InputProps>(
  (
    {
      type = "text",
      className,
      ringClasses,
      onFocus,
      value,
      onChange,
      style,
      autoComplete,
      autoCapitalize,
      ...rest
    },
    ref
  ) => {
    const [cursor, setCursor] = useState<number | null>(null);

    // TODO: does passing a ref work and not mess with cursor positioning?
    const inputRef = ref || useRef<HTMLInputElement | HTMLTextAreaElement>();

    const textarea = type === "textarea";
    const checkbox = type === "checkbox";
    const radio = type === "radio";
    const decimal = type === "decimal";
    const number = type === "number" || decimal;
    const inputMode = TYPE_INPUT_MODE_MAP[type];

    const autoFeaturesDisabled =
      !!inputMode && inputMode !== "text" && inputMode !== "search";

    const onChangeWrapper: FormEventHandler<
      HTMLInputElement | HTMLTextAreaElement
    > = (event) => {
      setCursor(
        (event.target as HTMLInputElement | HTMLTextAreaElement).selectionStart
      );

      onChange?.(event);
    };

    const onFocusWrapper: React.FocusEventHandler<
      HTMLInputElement | HTMLTextAreaElement
    > = (event) => {
      event.target.select();
      onFocus?.(event);
    };

    const props: InputHTMLAttributes<HTMLInputElement | HTMLTextAreaElement> = {
      autoCapitalize:
        autoCapitalize || autoFeaturesDisabled ? "none" : undefined,
      autoComplete: autoComplete || autoFeaturesDisabled ? "off" : undefined,
      ...rest,
      onFocus: onFocusWrapper,
      className: classNames(
        "bg-transparent disabled:bg-transparent disabled:opacity-60 disabled:cursor-not-allowed focus:outline-none",
        ringClasses ||
          "focus:ring-2 focus:ring-offset-white ring-offset-1 focus:ring-buttonOrangeBackground",
        textarea && "border-r",
        className || "rounded px-1 py-0.5 border",
        checkbox &&
          "bg-white grid place-content-center appearance-none w-4 h-4 border-2 border-blue-500 checked:border-[0.5rem] cursor-pointer hover:border-blue-600 transition-all before:bg-white before:content-[''] before:w-2 before:h-2 before:transition-all rounded",
        radio &&
          "bg-white hover:bg-gray-50 appearance-none rounded-full w-4 h-4 border-2 border-blue-500 checked:border-[0.35rem] cursor-pointer hover:border-blue-600 transition-all"
      ),
      type: inputMode ? "text" : type,
      inputMode,
      style: textarea ? { ...style, minHeight: "5rem" } : style,
      value: number && isNumber(value) && isNaN(value) ? "" : value,
      onChange: inputMode ? onChangeWrapper : onChange,
      // TODO: disallow inputting random characters for number input
      pattern: decimal ? "[0-9]+([.,][0-9]+)?" : number ? "[0-9]+" : undefined,
    };

    useEffect(() => {
      if (!inputMode) {
        return;
      }

      (
        inputRef as RefObject<HTMLInputElement | HTMLTextAreaElement>
      )?.current?.setSelectionRange?.(cursor, cursor);
    }, [value, cursor, inputRef]);

    if (textarea) {
      return (
        <textarea {...props} ref={inputRef as RefObject<HTMLTextAreaElement>} />
      );
    }

    return <input {...props} ref={inputRef as RefObject<HTMLInputElement>} />;
  }
);

Input.displayName = "Input";

export default Input;
