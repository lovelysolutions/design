/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef } from "react";

import "./input.css";
import { RadioGroup } from "@headlessui/react";
import { SelectOption } from "./Select";

export type RadioProps = Omit<React.HTMLProps<HTMLInputElement>, "onChange"> & {
  onChange?: (value: any) => void;
  options: SelectOption[];
};

const RadioInput = forwardRef<HTMLInputElement, RadioProps>(
  ({ value, onChange, options, ...rest }, ref) => {
    return (
      <RadioGroup
        {...rest}
        onChange={(value: any) => {
          onChange?.(value);
        }}
        value={value}
        as={undefined}
        ref={ref}
      >
        {options.map((option, index) => (
          <RadioGroup.Option
            value={option.value}
            key={index}
            className="flex flex-grow items-center gap-2 py-0.5"
          >
            <input type="radio" checked={value === option.value} />
            {option.name}
          </RadioGroup.Option>
        ))}
      </RadioGroup>
    );
  }
);

RadioInput.displayName = "RadioInput";

export default RadioInput;
