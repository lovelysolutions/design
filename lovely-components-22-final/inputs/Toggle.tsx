/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef } from "react";
import { Switch } from "@headlessui/react";
import omit from "lodash/omit";
import classNames from "classnames";

type ToggleProps = Omit<
  React.HTMLProps<HTMLButtonElement>,
  "onChange" | "value"
> & {
  onChange: (value: boolean) => void;
  value?: boolean;
  borderClassName?: string;
};

const Toggle = forwardRef<HTMLButtonElement, ToggleProps>(
  ({ className, children, value, onChange, borderClassName, ...rest }, ref) => {
    return (
      <Switch.Group>
        <div className={classNames("flex items-center gap-x-4", className)}>
          <Switch
            {...omit(rest, "as", "type")}
            onChange={onChange}
            checked={!!value}
            className={classNames(
              value ? "bg-blue-500" : "bg-gray-200",
              "relative inline-flex items-center h-6 rounded-full w-11 transition-colors disabled:opacity-50 disabled:cursor-not-allowed focus:outline-none focus:ring-2 focus:ring-offset-white ring-offset-1 focus:ring-buttonOrangeBackground border",
              borderClassName ? borderClassName : "border-transparent"
            )}
            ref={ref}
            type="button"
          >
            <span
              className={`${
                value ? "translate-x-6" : "translate-x-1"
              } inline-block w-4 h-4 transform bg-white rounded-full transition-transform`}
            />
          </Switch>
          {!!children && (
            <Switch.Label className="cursor-pointer">{children}</Switch.Label>
          )}
        </div>
      </Switch.Group>
    );
  }
);

Toggle.displayName = "Toggle";

export default Toggle;
