/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef, useState } from "react";

import "./input.css";
import Input, { InputProps } from "./Input";
import IconButton from "../buttons/IconButton";
import { faEyeSlash, faEye } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";

const PasswordInput = forwardRef<HTMLInputElement, InputProps>(
  ({ type, className, ...rest }, ref) => {
    const [passwordShown, setPasswordShown] = useState<boolean>(false);

    return (
      <div className="flex-grow relative flex items-center justify-center">
        <Input
          {...rest}
          ref={ref}
          type={passwordShown ? "text" : type}
          className={classNames(
            className || "rounded px-1 py-0.5 border",
            "pr-9"
          )}
        />
        <IconButton
          onClick={() => setPasswordShown(!passwordShown)}
          className="flex absolute right-0 px-2"
          tabIndex={-1}
          icon={passwordShown ? faEyeSlash : faEye}
        />
      </div>
    );
  }
);

PasswordInput.displayName = "PasswordInput";

export default PasswordInput;
