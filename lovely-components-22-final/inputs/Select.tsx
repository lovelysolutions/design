/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, {
  forwardRef,
  HTMLProps,
  useMemo,
  ReactNode,
  Fragment,
} from "react";
import { Listbox } from "@headlessui/react";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretUp, faCaretDown } from "@fortawesome/free-solid-svg-icons";

export type SelectOption = {
  key?: string | number;
  value: any;
  name: string;
  disabled?: boolean;
  element?: ReactNode;
};

export type SelectProps = Omit<HTMLProps<HTMLButtonElement>, "onChange"> & {
  options: SelectOption[];
  onChange: (value: any) => void;
  value: any;
  show?: boolean;
  isSelected?: (value?: any) => boolean;
  onBlur?: () => void;
  hideWhenEmpty?: boolean;
  showCaret?: boolean;
  noResultsText?: string;
};

const SELECT_OPTION_CLASSES = "px-3 py-1";

const Select = forwardRef<HTMLButtonElement, SelectProps>(
  (
    {
      options,
      children,
      show,
      value,
      disabled,
      onChange,
      isSelected,
      onBlur,
      onClick,
      hideWhenEmpty,
      showCaret,
      noResultsText = "No results",
    },
    ref
  ) => {
    if (hideWhenEmpty && !options.length) {
      return null;
    }

    const timestamp = useMemo(() => String(new Date().getTime()), []);

    return (
      <Listbox value={value} disabled={disabled} onChange={onChange}>
        {({ open }) => {
          const opened = show || open;

          return (
            <>
              <Listbox.Button
                ref={ref}
                onClick={onClick}
                className="flex items-center flex-nowrap gap-x-2"
              >
                {children}
                {!!showCaret && (
                  <div className={opened ? "text-textOrange" : ""}>
                    {opened ? (
                      <FontAwesomeIcon icon={faCaretUp} />
                    ) : (
                      <FontAwesomeIcon icon={faCaretDown} />
                    )}
                  </div>
                )}
              </Listbox.Button>
              {opened && (
                <Listbox.Options
                  static
                  as="div"
                  className="bg-white border rounded-md absolute top-full min-w-full z-10 divide-y overflow-y-auto divide-gray-100 list-none"
                  style={{ maxHeight: "20rem" }}
                  onBlur={onBlur}
                >
                  {!options.length && (
                    <div
                      className={classNames(
                        SELECT_OPTION_CLASSES,
                        "text-gray-500"
                      )}
                    >
                      {noResultsText}
                    </div>
                  )}
                  {options.map((option, optionIndex) => (
                    <Listbox.Option
                      value={option.value}
                      disabled={option.disabled}
                      key={
                        option.key ||
                        `dropdown-${timestamp}-option-${optionIndex}`
                      }
                    >
                      {({ active, selected }) => (
                        <div
                          className={classNames(
                            !!option.value && SELECT_OPTION_CLASSES,
                            !!option.value &&
                              !disabled &&
                              !option.disabled &&
                              "cursor-pointer",
                            !!option.value && "hover:bg-gray-100",
                            !!option.value && active && "bg-gray-100",
                            !!option.value &&
                              (selected || isSelected?.(option.value)) &&
                              "font-semibold",
                            !!option.value && option.disabled && "bg-gray-50"
                          )}
                        >
                          {option.element || option.name}
                        </div>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              )}
            </>
          );
        }}
      </Listbox>
    );
  }
);

Select.displayName = "Select";

export default Select;
