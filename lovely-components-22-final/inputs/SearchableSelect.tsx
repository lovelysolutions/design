/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, {
  forwardRef,
  useState,
  useCallback,
  useRef,
  useEffect,
} from "react";
import Input from "./Input";
import { InputProps } from "./Input";
import Select, { SelectProps, SelectOption } from "./Select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faTimes } from "@fortawesome/free-solid-svg-icons";
import Spinner from "../../loading/Spinner";
import debounce from "lodash/debounce";
import IconButton from "../buttons/IconButton";
import classNames from "classnames";
import isString from "lodash/isString";

export type SearchableSelectProps = Omit<SelectProps, "options" | "onBlur"> & {
  inputProps?: InputProps;
  search: (query: string) => Promise<SelectOption[]>;
  addOption?: (query: string) => SelectOption | undefined;
  clearable?: boolean;
  onClear?: () => void;
  skipSearchWhenNoQuery?: boolean;
  prependOptions?: (query: string) => SelectOption[] | undefined;
};

export type SearchableSelectFieldProps = Omit<
  SearchableSelectProps,
  "value" | "onChange"
>;

export const DefaultAddNewSelectOptionElement: React.FC = ({ children }) => (
  <div className="flex items-center gap-x-3 font-medium">
    <FontAwesomeIcon icon={faPlusCircle} className="text-textOrange" />
    <span>{children}</span>
  </div>
);

const SearchableSelect = forwardRef<HTMLInputElement, SearchableSelectProps>(
  (
    {
      inputProps,
      search,
      addOption,
      disabled,
      onChange,
      className,
      clearable,
      onClear,
      value = "",
      hideWhenEmpty,
      skipSearchWhenNoQuery,
      prependOptions,
      ...rest
    },
    ref
  ) => {
    const wrapperRef = useRef<HTMLDivElement>(null);
    const selectRef = useRef<HTMLButtonElement>(null);
    const [query, setQuery] = useState<string>(isString(value) ? value : "");
    const [open, setOpen] = useState<boolean>(false);
    const [saving, setSaving] = useState<boolean>(false);
    const [searching, setSearching] = useState<boolean>(false);
    const [options, setOptions] = useState<SelectOption[]>([]);

    const trimmedQuery = query.trim();
    const addableOption = addOption?.(trimmedQuery);

    const allOptions = [
      ...(addableOption
        ? [
            {
              element: (
                <DefaultAddNewSelectOptionElement>
                  Add {addableOption.name || "new"}
                </DefaultAddNewSelectOptionElement>
              ),
              ...addableOption,
            },
          ]
        : []),
      ...(prependOptions?.(query) || []),
      ...(searching
        ? [
            {
              name: "Loading...",
              value: null,
              disabled: true,
              element: <Spinner />,
            },
          ]
        : [...options]),
    ];

    useEffect(() => {
      if (!query) {
        return;
      }

      setOpen(query !== value);
    }, [query]);

    useEffect(() => {
      if (!value || !isString(value) || value === query) {
        return;
      }

      resetQuery();
      setOpen(false);
    }, [value]);

    const resetQuery = () => {
      const updatedQuery = isString(value) ? value : "";
      setQuery(updatedQuery);
      return updatedQuery;
    };

    const submitSearch = useCallback(
      debounce(async (updatedQuery: string) => {
        setSearching(true);
        try {
          // TODO: cancelable?
          const options = await search(updatedQuery);
          setOptions(options);
        } catch (error) {
          setOptions([]);
        }
        setSearching(false);
      }, 200),
      []
    );

    const setOpenDebounced = useCallback(
      debounce((open: boolean) => setOpen(open), 500, {
        leading: true,
        trailing: false,
      }),
      []
    );

    const onSearch = async (updatedQuery: string) => {
      setQuery(updatedQuery);
      if (
        skipSearchWhenNoQuery &&
        !updatedQuery.trim().length &&
        !options.length
      ) {
        search(updatedQuery);
        return;
      }
      setSearching(true);
      await submitSearch(updatedQuery);
    };

    const onChangeWrapper = async (update: any) => {
      setSaving(true);
      if (query) {
        setOptions([]);
      }
      resetQuery();
      await onChange(update);
      setSaving(false);
      setOpen(false);
    };

    return (
      <div
        className="flex-grow flex items-center relative"
        onClick={(event) => {
          event.stopPropagation();
          event.preventDefault();
        }}
        onBlur={(event) => {
          if (
            wrapperRef.current?.contains(event.relatedTarget as Node | null)
          ) {
            return;
          }
          setOpen(false);
          if (query) {
            setOptions([]);
          }
          resetQuery();
        }}
        ref={wrapperRef}
        onKeyDown={(event) => {
          if (event.key === "Escape") {
            setOpen(false);
          }
          if (event.key === "Enter") {
            event.preventDefault();
          }
        }}
      >
        {!!clearable && (
          <IconButton
            variant="link-plain"
            icon={faTimes}
            className="absolute right-0 p-2"
            onClick={() => (onClear ? onClear() : onChange(undefined))}
          />
        )}
        <Input
          {...inputProps}
          className={classNames(className, clearable && "pr-8")}
          value={query}
          onFocus={() => {
            if (!options.length) {
              onSearch(resetQuery());
            }
            setOpenDebounced(true);
          }}
          // TODO: fix
          // onBlur={() => setTimeout(() => setOpen(false))}
          disabled={disabled || saving}
          onChange={async (event) => {
            const query = (event.target as HTMLInputElement).value;

            onSearch(query);
          }}
          ref={ref}
          onKeyDown={(event) => {
            if (event.key == "ArrowDown") {
              selectRef.current?.focus();
            }
          }}
          onClick={() => setOpenDebounced(!open)}
        />
        <Select
          {...rest}
          onChange={onChangeWrapper}
          disabled={disabled || saving || searching}
          options={allOptions}
          show={open}
          ref={selectRef}
          value={value}
          hideWhenEmpty={
            (skipSearchWhenNoQuery || hideWhenEmpty) && !trimmedQuery.length
          }
          // onBlur={() => setOpen(false)}
        />
      </div>
    );
  }
);

SearchableSelect.displayName = "SearchableSelect";

export default SearchableSelect;
