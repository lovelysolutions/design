/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef, HTMLProps, ReactNode } from "react";
import classNames from "classnames";

import "./Tooltip.css";

type TooltipProps = Omit<HTMLProps<HTMLDivElement>, "content"> & {
  content: ReactNode;
};

const Tooltip = forwardRef<HTMLImageElement, TooltipProps>(
  ({ content, className, children, ...rest }, ref) => {
    return (
      <div
        {...rest}
        ref={ref}
        className={classNames("tooltip relative", className)}
        tabIndex={0}
      >
        <div className="tooltip__content bg-white rounded flex flex-col absolute top-full z-10 w-96 p-4 border shadow-xl right-1/2 transform translate-x-1/2 translate-y-1">
          {content}
        </div>
        {children}
      </div>
    );
  }
);

Tooltip.displayName = "Tooltip";

export default Tooltip;
