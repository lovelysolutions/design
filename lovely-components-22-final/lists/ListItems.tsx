/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import "./ListItems.css";

import React, {
  useState,
  ReactNode,
  Fragment,
  useEffect,
  useRef,
  ComponentType,
  HTMLProps,
  useMemo,
} from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import classNames from "classnames";
import isEqual from "lodash/isEqual";

import Spinner from "../../loading/Spinner";

import IconButton from "../buttons/IconButton";
import {
  faArrowUp,
  faCaretLeft,
  faCaretRight,
  faSearch,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import Input from "../inputs/Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "../images/Image";

import empty from "../../empty.png";
import Entity from "../api/Entity";
import Model from "../api/Model";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import DropdownFilter from "../dropdowns/DropdownFilter";
import FilterTag from "../../labels/FilterTag";
import {
  collectionFetch,
  FetchCollectionParams,
  CollectionBasePayload,
  FetchCollectionFunction,
  selectCollectionLoading,
  selectCollectionLastPage,
  selectCollectionPageNumber,
  selectCollectionPage,
  setCollectionLoading,
  updateCollectionPage,
  resetCollection,
  FullGetListParams,
  updateCollectionActiveFilters,
  selectCollectionFilters,
  CollectionFilterItem,
  resetCollectionState,
} from "../api/collectionSlice";
import { useHistory, useLocation } from "react-router-dom";
import { InputProps } from "../inputs/Input";
import { faCaretUp, faCaretDown } from "@fortawesome/free-solid-svg-icons";
import Button from "../buttons/Button";
import { selectCollectionNumberOfPages } from "../api/collectionSlice";
import range from "lodash/range";
import Field from "../forms/Field";

const DEFAULT_LIST_ITEM_CLASSES = "bg-white shadow";

export const VARIANT_DEFAULT: ListItemVariant = "default";
export const VARIANT_UNSTYLED: ListItemVariant = "unstyled";
export const VARIANT_MODAL: ListItemVariant = "modal";
export const VARIANT_FLAT: ListItemVariant = "flat";

type ListItemVariant = "default" | "unstyled" | "modal" | "flat";

const VARIANT_MAP: { [key: string]: string } = {
  [VARIANT_DEFAULT]: DEFAULT_LIST_ITEM_CLASSES,
  [VARIANT_MODAL]: "bg-modalListItem shadow",
  [VARIANT_FLAT]: "bg-white border",
};

const VARIANT_MAP_COLLAPSIBLE: { [key: string]: string } = {
  [VARIANT_DEFAULT]: "hover:bg-gray-50",
  [VARIANT_MODAL]: "hover:bg-backgroundOffWhite",
};

export interface Filter<
  T extends Entity = Entity,
  P extends Model<T> = Model<T>
> {
  name: string;
  key: string;
  type?: string;
  defaultItems?: CollectionFilterItem[];
  multiple?: boolean;
  listItemsProps: ListItemsProps<T, P>;
}

type CollapsibleListItemComponent = ComponentType<
  ListItemProps & {
    header: ReactNode;
    startExpanded?: boolean;
  }
>;
export const CollapsibleListItem: CollapsibleListItemComponent = ({
  children,
  header,
  startExpanded,
  variant = VARIANT_DEFAULT,
}) => {
  const [expanded, setExpanded] = useState<boolean>(startExpanded || false);

  return (
    <div
      className={classNames(
        "flex flex-col rounded shadow",
        VARIANT_MAP[variant]
      )}
    >
      <Button
        variant="unstyled"
        className={classNames(
          "flex items-center p-4",
          expanded && "border-b",
          VARIANT_MAP_COLLAPSIBLE[variant]
        )}
        onClick={() => setExpanded(!expanded)}
      >
        <div className="flex-grow flex">{header}</div>
        <FontAwesomeIcon icon={expanded ? faCaretUp : faCaretDown} />
      </Button>
      {expanded && (
        <div className="flex-grow flex flex-col p-4">{children}</div>
      )}
    </div>
  );
};

export const UnstyledListItem = <Q, T = any>({
  ...rest
}: ListItemProps<Q, T>) => <ListItem {...rest} variant={VARIANT_UNSTYLED} />;

export const ModalListItem = <Q, T = any>({ ...rest }: ListItemProps<Q, T>) => {
  return <ListItem {...rest} variant={VARIANT_MODAL} />;
};

export const FlatListItem = <Q, T = any>({ ...rest }: ListItemProps<Q, T>) => {
  return <ListItem {...rest} variant={VARIANT_FLAT} />;
};

export const ListItem = <Q, T = any>({
  children,
  className,
  draggable,
  dragging,
  item,
  onClick = item?.onClick,
  footer = item?.footer,
  variant = item?.variant || VARIANT_DEFAULT,
}: ListItemProps<Q, T>) => {
  const unstyled = variant === VARIANT_UNSTYLED;

  const WrapperComponent: React.ElementType<any> = unstyled
    ? Fragment
    : item?.to
    ? Link
    : // : item?.select
    // ? "label"
    item?.href
    ? "a"
    : "div";

  const clickable =
    !unstyled && !!(onClick || item?.to || item?.href || item?.select);

  const props: any = {
    ...(unstyled
      ? {}
      : item?.to
      ? { to: item?.to }
      : item?.href
      ? { href: item?.href }
      : { onClick }),
    className: unstyled
      ? undefined
      : classNames(
          "list-items__list-item",
          "flex flex-nowrap relative rounded",
          !footer && !item?.footer && "py-3 px-4",
          draggable && (dragging ? "cursor-grabbing" : "cursor-grab"),
          clickable && "cursor-pointer",
          VARIANT_MAP[variant],
          className || item?.className || "items-center"
        ),
  };

  if (item?.select && !onClick) {
    props.onClick = () => item.select?.();
  }

  if (unstyled) {
    delete props.className;
  }

  return (
    <WrapperComponent {...props}>
      {children || item?.content}
      {/* {children} */}
      {clickable && (
        <div className="absolute left-0 top-0 bottom-0 bg-backgroundBlue rounded-l list-items__list-item__border" />
      )}
    </WrapperComponent>
  );
};

type ListItemProps<Q = any | undefined, T = any | undefined> = HTMLProps<
  HTMLDivElement | HTMLAnchorElement | HTMLLabelElement
> & {
  item?: Item<Q, T> | Item<Q>;
  itemIndex?: number;
  componentProps?: Q;
  dragging?: boolean;
  updateItem?: (value: any) => void;
  deleteItem?: () => void;
  listLength?: number;
  collectionId?: string;
  refreshCollection?: () => Promise<void>;
  footer?: boolean;
  variant?: ListItemVariant;
};

export type ListItemComponent<
  Q = any | undefined,
  T = any | undefined
> = ComponentType<ListItemProps<Q, T>>;

export interface FetchItemsProps {
  page?: number;
  query?: string;
  [key: string]: any;
}

export interface Item<Q = any | undefined, T = any | undefined> {
  name?: string;
  value?: T;
  id?: string;
  Component?: ListItemComponent<Q, T> | ListItemComponent<Q>;
  content?: ReactNode;
  to?: string;
  onClick?: () => void;
  className?: string;
  selected?: boolean;
  select?: () => void;
  href?: string;
  footer?: boolean;
  variant?: ListItemVariant;
}

interface ConnectedListOnSortProps<
  T extends Entity = Entity,
  P extends Model = Model
> {
  fromIndex: number;
  toIndex: number;
  list: T[];
  ModelType?: new (...args: any[]) => P;
}

export const onConnectedListSort: <
  T extends Entity = Entity,
  P extends Model = Model
>(
  props: ConnectedListOnSortProps<T, P>
) => T[] = ({ fromIndex, toIndex, list, ModelType }) => {
  let updatedList = [...list];

  const from = { ...updatedList[fromIndex] };

  updatedList.splice(fromIndex, 1);
  updatedList.splice(toIndex, 0, from);

  updatedList = updatedList.map((item, index) => {
    const updated = {
      ...item,
      sortOrder: index + 1,
    };

    // Order didn't change
    if (updated.id === list[index]?.id) {
      return updated;
    }

    if (updated.id && ModelType) {
      new ModelType({ ...updated }).save();
    }

    return updated;
  });

  return updatedList;
};

export type ListItemsProps<
  T extends Entity = Entity,
  P extends Model<T> = Model<T>,
  Q = any | undefined
> = {
  disabled?: boolean;
  className?: string;
  dependencies?: any[];
  pagination?: boolean;
  search?: boolean;
  id: string;
  ItemComponent?: ListItemComponent<Q, T> | ListItemComponent<Q>;
  componentProps?: Q;
  actions?: ReactNode;
  filters?: Filter[];
  filterClasses?: string;
  searchClasses?: string;
  wrapperClasses?: string;
  fullLength?: boolean;
  sortBy?: string;
  sortable?: boolean;
  onSort?: (list: T[]) => void;
  filterBy?: (item: Item<Q, T>) => boolean;
  emptyState?: ReactNode;
  ModelType?: new <T>(
    idOrPartial?: number | Partial<T> | null | undefined
  ) => P;
  children?: ReactNode;
  pageSize?: number;
  params?: FetchCollectionParams;
  constructItem?: (entity: T) => Item<Q, T>;
  items?: (Item<Q, T> | Item<Q>)[];
  fetchEntities?: FetchCollectionFunction<T>;
  fetchItems?: (
    params: FullGetListParams
  ) => (Item<Q, T> | Item<Q>)[] | Promise<(Item<Q, T> | Item<Q>)[]>;
  cache?: boolean;
  selected?: (entity: T) => boolean;
  select?: (entity: T) => void;
  searchInputProps?: InputProps;
  prependItems?: (query: string) => Item<Q, T>[] | undefined;
  appendItems?: (query: string) => Item<Q, T>[] | undefined;
  plural?: string;
  singular?: string;
  canAddNew?: boolean;
};

interface FilterGroup {
  items: Item[];
  filter: Filter;
}

const ListItems = <
  T extends Entity = Entity,
  P extends Model<T> = Model<T>,
  Q = any | undefined
>({
  id,
  className,
  pagination,
  filters,
  search,
  actions,
  ItemComponent,
  componentProps,
  children,
  filterClasses,
  searchClasses,
  // fullLength,
  onSort,
  sortable = !!onSort,
  emptyState,
  dependencies,
  ModelType,
  params,
  constructItem,
  pageSize,
  items,
  fetchItems,
  fetchEntities,
  sortBy,
  filterBy,
  cache,
  selected,
  select,
  wrapperClasses,
  searchInputProps,
  prependItems,
  appendItems,
  plural,
  singular,
  canAddNew,
}: ListItemsProps<T, P, Q>) => {
  const { url } = useRouteMatch();

  const mounted = useRef(false);

  const staticList = !!fetchItems || !!items;
  if (!fetchItems && !items && !fetchEntities && !ModelType) {
    throw new Error(
      "ModelType required when not passing fetchItems, items, or fetchEntities"
    );
  }

  const history = useHistory();
  const { search: urlParams } = useLocation();
  const searchParams = new URLSearchParams(urlParams);
  const refresh = searchParams.get("refresh");

  const listRef = useRef<HTMLDivElement>(null);
  const [dragging, setDragging] = useState<boolean>(false);

  const dispatch = useAppDispatch();

  const payload: CollectionBasePayload = {
    id,
  };

  const collectionLoading = useAppSelector(selectCollectionLoading(payload));
  const lastPage = useAppSelector(selectCollectionLastPage(payload));
  const currentPage = useAppSelector(selectCollectionPageNumber(payload));
  const numberOfPages = useAppSelector(selectCollectionNumberOfPages(payload));
  // const collection = useAppSelector(selectCollection(payload));
  const defaultFilters = useMemo(
    () =>
      (filters || []).reduce((result, filter) => {
        if (!filter.defaultItems?.length) {
          return result;
        }

        return {
          ...result,
          [filter.key]: filter.defaultItems,
        };
      }, {}),
    []
  );
  const activeFilters =
    useAppSelector(selectCollectionFilters(payload)) || defaultFilters;

  const [query, setQuery] = useState<string>("");
  const [fetchedItems, setFetchedItems] = useState<Item<Q, T>[] | undefined>(
    fetchItems ? [] : undefined
  );

  const loading = staticList ? false : collectionLoading;

  const mapEntitiesToItems = (entities: T[]) =>
    entities.map((entity, index) => {
      const item: Item<Q, T> = {
        value: entity,
        // TODO: need to delete items without id after saves?
        id: entity.id ? String(entity.id) : `${id}-${index}`,
        ...constructItem?.(entity),
        selected: selected?.(entity),
        select: select ? () => select(entity) : undefined,
      };

      return item;
    });

  let collectionPage: Item<Q, T>[] = [
    ...(prependItems?.(query) || []),
    ...(fetchedItems ||
      items ||
      mapEntitiesToItems(
        useAppSelector(selectCollectionPage<T>(payload)) || []
      )),
    ...(appendItems?.(query) || []),
  ];

  // TODO: move to collection slice?
  // TODO: useMemo?
  if (sortBy) {
    collectionPage = collectionPage.sort(
      (a: any, b: any) => (a.value?.[sortBy] || 0) - (b.value?.[sortBy] || 0)
    );
  }

  // TODO: move to collection slice?
  // TODO: useMemo?
  if (filterBy) {
    collectionPage = collectionPage.filter(filterBy);
  }

  // const extendFullLength = !sortable && (!!fullLength || !!pagination);

  const draggable = !!sortable;

  useEffect(() => {
    if (url !== location.pathname || (refresh !== "true" && refresh !== id)) {
      return;
    }
    refreshCollection();
    history.replace({ search: "" });
  }, [refresh]);

  useEffect(() => {
    fetchCollectionIfValid();
  }, [dependencies, params, query, activeFilters, items, fetchItems, id]);

  useEffect(() => {
    setQuery("");
  }, [id]);

  // useEffect(() => {
  //   if (!dependencies?.length) {
  //     return;
  //   }

  //   fetchCollectionIfValid(true);
  // }, dependencies || []);

  useEffect(() => {
    mounted.current = true;

    return () => {
      mounted.current = false;

      if (cache || staticList) {
        return;
      }
      dispatch(resetCollection(payload));
    };
  }, []);

  const fetchAndSetItems = async () => {
    if (!fetchItems) {
      return;
    }
    const fetched = await fetchItems({ query });
    if (mounted.current) {
      setFetchedItems(fetched);
    }
  };

  const fetchCollectionIfValid = (refresh?: boolean) => {
    if (items) {
      return;
    }

    if (dependencies && !dependencies.every(Boolean)) {
      return;
    }

    if (fetchItems) {
      fetchAndSetItems();
      return;
    }
    // TODO: needs to be debounced?

    fetchCollection(currentPage, refresh);
  };

  const fetchCollection = async (page?: number, refresh?: boolean) => {
    // TODO: is this the right spot?
    scrollListToTop();

    await dispatch(
      collectionFetch({
        id,
        params: { ...params, ...filterProps },
        fetch: fetchEntities || fetch,
        query,
        page,
        pageSize,
        fixed: !pagination,
        refresh,
        filters: activeFilters,
      })
    );
  };

  const refreshCollection = async () => {
    await dispatch(resetCollectionState(payload));
    fetchCollectionIfValid();
  };

  const fetch: FetchCollectionFunction<T> = async (params) => {
    // TODO: cleaner way to do this?
    const ModelCast: typeof Model = ModelType as any;
    const { data, total } = await ModelCast.getList<P>({ params });

    const list = data.map((model) => ({ ...model.entity })) as T[];

    return {
      list,
      total,
    };
  };

  const scrollListToTop = () => {
    if (!pagination) {
      return;
    }

    listRef.current?.scrollTo(0, 0);
  };

  const getFilterGroups = () => {
    const groups: FilterGroup[] = [];
    for (const [filterKey, items] of Object.entries(activeFilters)) {
      const filter = filters?.find((filter) => filter.key === filterKey);

      if (!filter || !items || !items.length) {
        continue;
      }

      groups.push({
        items,
        filter,
      });
    }
    return groups;
  };

  const filterGroups = useMemo(getFilterGroups, [activeFilters]);

  const filterProps = useMemo(() => {
    return filterGroups.reduce(
      (group, { items, filter }) => ({
        ...group,
        [filter.key]: filter.multiple
          ? items.map((item) => item?.value)
          : items[0]?.value,
      }),
      {}
    );
  }, [filterGroups]);

  const updateItem = (_item: Item<Q, T>, itemIndex: number) => (value: T) => {
    const updatedList: T[] = collectionPage.map((item) => item.value) as T[];
    updatedList.splice(itemIndex, 1, value);

    dispatch(updateCollectionPage({ ...payload, list: updatedList }));
  };

  const deleteItem = (item: Item<Q, T>, itemIndex: number) => () => {
    const updatedList = collectionPage.map(
      (collectionItem) => collectionItem.value
    ) as T[];
    updatedList.splice(itemIndex, 1);

    // TODO: remove this? Give option
    if ((item.value as T).id && ModelType) {
      new ModelType(item.value).delete();
    }

    dispatch(updateCollectionPage({ ...payload, list: updatedList }));
  };

  const list = (
    <div
      className={classNames(
        "flex flex-col flex-grow",
        className || "gap-y-3",
        // extendFullLength && "pb-2 overflow-y-auto overflow-x-hidden",
        (loading || !collectionPage.length) && "hidden"
      )}
      ref={listRef}
    >
      {collectionPage.map((item, itemIndex) => {
        const key = `${id}-list-item-${item.id || `index-${itemIndex}`}`;

        const WrapperComponent: ListItemComponent<Q, T> =
          item?.Component || ItemComponent || ListItem;

        const innerContent = (
          <WrapperComponent
            key={draggable ? undefined : key}
            item={item}
            itemIndex={itemIndex}
            componentProps={componentProps}
            dragging={dragging}
            draggable={draggable}
            updateItem={updateItem(item, itemIndex)}
            deleteItem={deleteItem(item, itemIndex)}
            listLength={collectionPage.length}
            collectionId={id}
            refreshCollection={refreshCollection}
          />
        );

        return draggable ? (
          <Draggable key={key} draggableId={key} index={itemIndex}>
            {(provided) => (
              <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                // TODO: only if in modal?
                style={{ ...provided.draggableProps.style, left: 0 }}
              >
                {innerContent}
              </div>
            )}
          </Draggable>
        ) : (
          innerContent
        );
      })}
    </div>
  );

  const onQueryChange = (updatedQuery: string) => {
    setQuery(updatedQuery);
  };

  const onFilter = (
    filter: Filter,
    filteredItems: CollectionFilterItem[] = []
  ) => {
    if (!mounted.current) {
      return;
    }

    if (!isEqual(activeFilters[filter.key], filteredItems)) {
      dispatch(
        updateCollectionActiveFilters({
          ...payload,
          filters: {
            ...activeFilters,
            [filter.key]: filteredItems,
          },
        })
      );
    }
  };

  const removeFilter = (filter: Filter, item: Item) => {
    onFilter(
      filter,
      activeFilters[filter.key]?.filter((activeItem) => activeItem !== item)
    );
  };

  return (
    <div
      className={classNames(
        wrapperClasses || "flex-grow flex flex-col gap-y-3"
        // extendFullLength && "overflow-hidden overflow-y-visible"
      )}
      key={`list-items-${id}`}
    >
      {(!!filters?.length || search || actions) && (
        <div className={classNames("flex items-start gap-x-4", filterClasses)}>
          <div className="flex items-center gap-x-4 flex-wrap flex-grow">
            {!!filters?.length && (
              <Fragment>
                {filters.map((filter, index) => (
                  <div key={`${id}-filters-${filter.key}`}>
                    <DropdownFilter
                      id={`${id}-filters-${index}`}
                      filter={filter}
                      onFilter={(filteredItems) =>
                        onFilter(filter, filteredItems)
                      }
                      activeItems={activeFilters[filter.key]}
                    >
                      {filter.name}
                    </DropdownFilter>
                  </div>
                ))}
              </Fragment>
            )}
            {search && (
              <label className="flex items-center relative cursor-pointer flex-grow">
                <a
                  // TODO: force refresh?
                  className="block absolute px-2.5 select-none"
                >
                  <FontAwesomeIcon
                    icon={faSearch}
                    // size="sm"
                  />
                </a>
                {query && (
                  <a
                    onClick={() => onQueryChange("")}
                    className="block absolute right-0 px-2.5 select-none text-gray-400"
                  >
                    <FontAwesomeIcon
                      icon={faTimes}
                      // size="sm"
                    />
                  </a>
                )}
                <Input
                  className={classNames(
                    "flex-grow pl-9 py-1 text-sm font-medium leading-7",
                    !query && "border-transparent",
                    query && "pr-9",
                    searchClasses || "border-b focus:border-gray-200"
                  )}
                  ringClasses="focus:ring-0 focus:ring-offset-white ring-offset-0 focus:ring-buttonOrangeBackground"
                  placeholder="Search by name"
                  onChange={(event) =>
                    onQueryChange((event.target as HTMLInputElement).value)
                  }
                  value={query}
                  type="search"
                  key={`${id}-search`}
                  {...searchInputProps}
                  ref={null}
                />
              </label>
            )}
          </div>
          {!!actions && <div className="ml-auto">{actions}</div>}
        </div>
      )}
      {!!filterGroups.length && (
        <div className="flex gap-x-2">
          {filterGroups.map(({ filter, items: filterItems }, index) =>
            filterItems.map((item, itemIndex) => (
              <FilterTag
                key={`${id}-active-filter-${index}-item-${itemIndex}`}
                onRemove={() => removeFilter(filter, item)}
              >
                {item.name || item.value}
              </FilterTag>
            ))
          )}
        </div>
      )}
      {!loading &&
        !collectionPage.length &&
        (emptyState || (
          <div
            className={classNames(
              DEFAULT_LIST_ITEM_CLASSES,
              "pt-2 pb-10 px-4 flex flex-col gap-y-2 relative"
            )}
          >
            <div className="mx-auto h-48">
              <Image src={empty} alt="List is empty" fit="contain" />
            </div>
            <div className="font-medium text-black text-center text-xl">
              {"Zero, zip, zilch, nada."}
            </div>
            <div className="font-medium text-gray-400 text-center">
              {`We couldn't find ${
                plural ? `any ${plural}` : "anything"
              } for this search`}
            </div>
            {!!canAddNew && (
              <div className="absolute top-2 right-0 text-xs text-gray-400 flex items-center pl-2">
                <span>
                  {`Click the plus button to add a new ${singular || "item"}`}
                </span>
                <div className="w-10 flex justify-center flex-shrink-0">
                  <FontAwesomeIcon icon={faArrowUp} />
                </div>
              </div>
            )}
          </div>
        ))}
      {loading && (
        <div>
          <Spinner className="py-24" />
        </div>
      )}

      {sortable ? (
        <DragDropContext
          onBeforeCapture={() => !!mounted.current && setDragging(true)}
          onDragEnd={async (result) => {
            if (!mounted.current) {
              return;
            }

            setDragging(false);

            if (
              !result.destination ||
              result.destination.index === result.source.index
            ) {
              return;
            }

            // TODO: separate sorting flag?
            await dispatch(setCollectionLoading(payload));

            const entities = collectionPage.map((item) => item.value) as T[];

            const updatedCollectionPage = onConnectedListSort<T>({
              fromIndex: result.source.index,
              toIndex: result.destination.index,
              list: entities,
              ModelType,
            });

            if (onSort) {
              onSort?.(updatedCollectionPage);
            }

            await dispatch(
              updateCollectionPage({
                ...payload,
                list: updatedCollectionPage,
              })
            );

            // TODO: show successful sort toast
          }}
        >
          <Droppable droppableId={`droppable-${id}`} type={id}>
            {(provided) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className={classNames(
                  (loading || !collectionPage.length) && "hidden"
                )}
              >
                {list}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      ) : (
        list
      )}
      {!loading &&
        !!pagination &&
        (!!collectionPage.length || currentPage !== 1) &&
        (currentPage !== 1 || !lastPage) && (
          <div className="flex items-center space-x-2">
            <div className="flex-grow text-sm font-medium text-gray-400 px-2">
              {/* TODO: out of? */}
              {`You are viewing page ${currentPage}${
                numberOfPages ? ` out of ${numberOfPages}` : ""
              }`}
            </div>
            <div className="flex ml-auto items-center">
              <IconButton
                icon={faCaretLeft}
                className="p-4"
                size="lg"
                onClick={() => fetchCollection(currentPage - 1)}
                disabled={loading || currentPage === 1}
              />
              <div className="relative select-none w-8">
                <label
                  className={classNames(
                    "absolute text-xs text-center top-0 text-gray-500 transform -translate-y-full left-1/2 -translate-x-1/2 leading-5",
                    numberOfPages && "cursor-pointer"
                  )}
                  htmlFor={numberOfPages ? "number-of-pages" : undefined}
                >
                  Page
                </label>
                <div className="text-sm text-center px-2 font-semibold">
                  {numberOfPages ? (
                    <Field
                      type="nativeSelect"
                      id="number-of-pages"
                      options={range(1, numberOfPages + 1).map((page) => ({
                        name: String(page),
                        value: +page,
                      }))}
                      editing
                      value={currentPage}
                      onChange={(page) => fetchCollection(+page)}
                      inputClassName="text-center px-0.5 box-content h-4 rounded-none font-semibold"
                      inputClassNameOverride
                    />
                  ) : (
                    currentPage
                  )}
                </div>
              </div>
              <IconButton
                icon={faCaretRight}
                className="p-4"
                size="lg"
                onClick={() => fetchCollection(currentPage + 1)}
                disabled={loading || lastPage}
              />
            </div>
          </div>
        )}
      {children}
    </div>
  );
};

export default ListItems;
