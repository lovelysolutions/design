/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef, ReactNode, useEffect, useState } from "react";
import classNames from "classnames";

export type ImageProps = Omit<
  React.HTMLProps<HTMLImageElement>,
  "crossOrigin"
> & {
  backupSrc?: string;
  backup?: ReactNode;
  fit?: "contain" | "cover" | "fill";
  backgroundClassName?: string;
};

const TRANSPARENT_PIXEL =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

const Image = forwardRef<HTMLImageElement, ImageProps>(
  (
    {
      className,
      src,
      backupSrc,
      backup,
      fit = "cover",
      backgroundClassName = "bg-textBlueGrayAgainstBlue",
      onLoad,
      ...rest
    },
    ref
  ) => {
    const [loaded, setLoaded] = useState<boolean>(false);
    const [failed, setFailed] = useState<boolean>(false);

    const failedSrc = backupSrc || TRANSPARENT_PIXEL;

    useEffect(() => {
      if (loaded) {
        setLoaded(false);
      }
      if (failed) {
        setFailed(false);
      }
    }, [src]);

    return (
      <div
        className={classNames(
          "w-full h-full",
          className,
          (!loaded || failed) && backgroundClassName
        )}
      >
        {failed && backup ? (
          <span className="animate-fade-in">{backup}</span>
        ) : (
          <img
            {...rest}
            ref={ref}
            onLoad={(event) => {
              setLoaded(true);
              onLoad?.(event);
            }}
            className={classNames(
              !loaded && !failed ? "opacity-0" : "opacity-100",
              "w-full h-full before:hidden transition-opacity",
              `object-${fit}`
            )}
            src={failed ? failedSrc : src}
            onError={() => {
              setLoaded(false);
              setFailed(true);
            }}
          />
        )}
      </div>
    );
  }
);

Image.displayName = "Image";

export default Image;
