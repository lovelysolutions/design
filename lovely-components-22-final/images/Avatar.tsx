/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, { forwardRef } from "react";
import Image, { ImageProps } from "./Image";
import classNames from "classnames";

export type AvatarProps = Omit<ImageProps, "size"> & {
  sizeClass?: string;
};

const Avatar = forwardRef<HTMLImageElement, AvatarProps>(
  ({ sizeClass = "w-12 h-12", ...rest }, ref) => {
    return (
      <div
        className={classNames(
          "rounded-full overflow-hidden flex-shrink-0",
          sizeClass
        )}
      >
        <Image {...rest} ref={ref} />
      </div>
    );
  }
);

Avatar.displayName = "Avatar";

export default Avatar;
