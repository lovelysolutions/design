/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import {
  CancelablePromise,
  makeCancelable,
  CanceledPromiseError,
} from "../../utilities/promises";
import Entity from "./Entity";
import isNumber from "lodash/isNumber";
import reduce from "lodash/reduce";
import isEqual from "lodash/isEqual";

export interface CollectionFilterItem {
  name: string;
  value: any;
}
export interface CollectionFilters {
  [key: string]: undefined | CollectionFilterItem[];
}

type Status = "loading" | "failed" | "deleted";
export const STATUS_LOADING = "loading";
export const STATUS_FAILED = "failed";
export const STATUS_DELETED = "deleted";

export interface CollectionBasePayload {
  id: string;
}

export type CollectionGetPayload = CollectionBasePayload & {
  page?: number;
};

export type UpdateCollectionActiveFiltersPayload = CollectionBasePayload & {
  filters?: CollectionFilters;
};

export type UpdateCollectionPagePayload = CollectionBasePayload & {
  page?: number;
  list: Entity[];
};

export type UpdatePromisePayload = CollectionBasePayload & {
  promise?: CancelablePromise;
};

export interface FetchCollectionParams {
  [key: string]: any;
}

export interface FullGetListParams {
  query?: string;
  page?: number;
}

type StatusUpdate = CollectionBasePayload & {
  status?: Status;
};

type CollectionUpdate<T extends Entity = Entity> = CollectionBasePayload & {
  page: number;
  list: T[];
  total?: number;
  pageSize?: number;
  params?: FetchCollectionParams;
  query?: string;
  fixed?: boolean;
  filters?: CollectionFilters;
};

type PageUpdate = CollectionBasePayload & {
  page: number;
};

interface CollectionCache {
  [id: string]: CollectionState | undefined;
}

const initialState: CollectionCache = {};

interface CollectionPageCache<T extends Entity = Entity> {
  [page: number]: T[];
}

interface CollectionState {
  id?: string;
  cache: CollectionPageCache;
  status?: Status;
  page?: number;
  query?: string;
  pageSize?: number;
  total?: number;
  params?: FetchCollectionParams;
  fixed?: boolean;
  filters?: CollectionFilters;
}

const initialCollectionState: CollectionState = {
  cache: {},
  params: {},
};

interface FetchData<T extends Entity = Entity> {
  list: T[];
  total?: number;
}

export type FetchCollectionFunction<T extends Entity = Entity> = (
  props: FullGetListParams & FetchCollectionParams
) => Promise<FetchData<T>>;

export type CollectionSaveAction<T extends Entity = Entity> =
  CollectionBasePayload & {
    save: (list: T[]) => Promise<T[]>;
  };

export type CollectionAddAction<T extends Entity = Entity> =
  CollectionBasePayload & {
    instantiate: (list: T[]) => T;
  };

export type CollectionFetchAction<T extends Entity = Entity> =
  CollectionBasePayload & {
    fetch: FetchCollectionFunction<T>;
    pageSize?: number;
    page?: number;
    query?: string;
    params?: FetchCollectionParams;
    fixed?: boolean;
    refresh?: boolean;
    filters?: CollectionFilters;
  };

export const collectionSavePage = createAsyncThunk(
  "collection/savePage",
  async (action: CollectionSaveAction, { dispatch, getState }) => {
    const state = getState() as RootState;
    const payload = { id: action.id };
    let list = selectCollectionPage(payload)(state) || [];

    await dispatch(setCollectionStatus({ ...payload, status: STATUS_LOADING }));

    list = await action.save(list);

    await dispatch(
      updateCollectionPage({
        ...payload,
        list,
      })
    );

    await dispatch(setCollectionStatus({ ...payload }));
  }
);

export const collectionAdd = createAsyncThunk(
  "collection/add",
  async (action: CollectionAddAction, { dispatch, getState }) => {
    const state = getState() as RootState;
    const payload = { id: action.id };
    const list = selectCollectionPage(payload)(state) || [];
    await dispatch(
      updateCollectionPage({
        ...payload,
        list: [...list, action.instantiate(list)],
      })
    );
  }
);

export const collectionFetch = createAsyncThunk(
  "collection/fetch",
  async (action: CollectionFetchAction, { dispatch, getState }) => {
    const state = getState() as RootState;
    const payload = { id: action.id };

    try {
      const currentPage = selectCollectionPageNumber(payload)(state);
      const collection = selectCollection(payload)(state);

      const oldQuery = collection?.query;
      const query = action.query;
      const queryChanged = query != oldQuery;

      const previousParams = collection?.params;
      const params = action.params;

      const filters = action.filters || collection?.filters;

      const paramsChanged = !isEqual(previousParams, params);

      const changed =
        action.refresh || !collection || queryChanged || paramsChanged;

      const page = changed
        ? 1
        : action.page ||
          (action.fixed
            ? 1
            : collection?.cache[currentPage]
            ? currentPage + 1
            : currentPage);

      await dispatch(updatePage({ ...payload, page }));

      if (
        !changed &&
        (collection?.cache[page] || selectCollectionLoading(payload)(state))
      ) {
        return;
      }

      if (changed && collection) {
        await dispatch(resetCollectionState(payload));
      }

      await dispatch(
        setCollectionStatus({ ...payload, status: STATUS_LOADING })
      );

      const fetch = action.fetch;
      const fixed = action.fixed;

      const promise = makeCancelable(
        fetch({
          page,
          query,
          ...params,
        })
      );

      updatePromise({ ...payload, promise });

      const { list, total } = (await promise.promise) as FetchData;

      await dispatch(
        updateCollection({
          ...payload,
          pageSize: action.pageSize,
          list,
          page,
          total,
          params,
          query,
          fixed,
          filters,
        })
      );
    } catch (error) {
      if ((error as CanceledPromiseError | null)?.isCanceled) {
        return;
      }
      await dispatch(
        setCollectionStatus({ ...payload, status: STATUS_FAILED })
      );
      throw error;
    }
  }
);

const ensureCollectionState = (state: CollectionCache, id: string) => {
  if (!state[id]) {
    state[id] = { ...initialCollectionState, id };
  }
};

const assumePageSize = (collectionSize: number) => {
  if (collectionSize > 20) {
    return 25;
  }

  if (collectionSize === 20) {
    return 20;
  }

  if (collectionSize === 15) {
    return 15;
  }

  return 10;
};

interface PromiseMap {
  [key: string]: CancelablePromise | undefined;
}

// TODO: where else can this go?
const promiseMap: PromiseMap = {};

const updatePromise = (payload: UpdatePromisePayload) => {
  promiseMap[payload.id]?.cancel();
  promiseMap[payload.id] = payload.promise;
};

export const collectionSlice = createSlice({
  name: "collection",
  initialState,
  reducers: {
    resetCollections: () => ({ ...initialState }),
    updateCollectionPage: (
      state: CollectionCache,
      action: PayloadAction<UpdateCollectionPagePayload>
    ) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.cache![
        action.payload.page || state[action.payload.id]?.page || 1
      ] = action.payload.list;
      state[action.payload.id]!.status = undefined;
    },
    updateCollectionActiveFilters: (
      state: CollectionCache,
      action: PayloadAction<UpdateCollectionActiveFiltersPayload>
    ) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.filters = action.payload.filters;
    },
    resetCollection: (
      state: CollectionCache,
      action: PayloadAction<CollectionBasePayload>
    ) => {
      updatePromise(action.payload);
      state[action.payload.id] = undefined;
    },
    resetCollectionState: (
      state: CollectionCache,
      action: PayloadAction<CollectionBasePayload>
    ) => {
      updatePromise(action.payload);
      state[action.payload.id] = state[action.payload.id]
        ? {
            ...state[action.payload.id],
            id: action.payload.id,
            cache: {},
            page: undefined,
            params: {},
            status: undefined,
            total: undefined,
            pageSize: undefined,
          }
        : undefined;
    },
    setCollectionStatus: (
      state: CollectionCache,
      action: PayloadAction<StatusUpdate>
    ) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.status = action.payload.status;
    },
    setCollectionLoading: (
      state: CollectionCache,
      action: PayloadAction<CollectionBasePayload>
    ) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.status = STATUS_LOADING;
    },
    setCollectionDoneLoading: (
      state: CollectionCache,
      action: PayloadAction<CollectionBasePayload>
    ) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.status = undefined;
    },
    updateCollection: (
      state: CollectionCache,
      action: PayloadAction<CollectionUpdate>
    ) => {
      ensureCollectionState(state, action.payload.id);

      state[action.payload.id]!.cache[action.payload.page] =
        action.payload.list;

      if (isNumber(action.payload.total)) {
        state[action.payload.id]!.total = action.payload.total;
      }

      if (
        action.payload.page === 1 &&
        !isNumber(state[action.payload.id]!.pageSize)
      ) {
        state[action.payload.id]!.pageSize =
          action.payload.pageSize || assumePageSize(action.payload.list.length);
      }

      state[action.payload.id]!.id = action.payload.id;
      state[action.payload.id]!.params = action.payload.params;
      state[action.payload.id]!.query = action.payload.query;
      state[action.payload.id]!.page = action.payload.page;
      state[action.payload.id]!.fixed = action.payload.fixed;
      state[action.payload.id]!.filters = action.payload.filters;
      state[action.payload.id]!.status = undefined;

      promiseMap[action.payload.id] = undefined;
    },
    updatePage: (state: CollectionCache, action: PayloadAction<PageUpdate>) => {
      ensureCollectionState(state, action.payload.id);
      state[action.payload.id]!.page = action.payload.page;
    },
  },
});

export const {
  resetCollections,
  resetCollection,
  resetCollectionState,
  setCollectionStatus,
  updateCollection,
  updatePage,
  setCollectionLoading,
  setCollectionDoneLoading,
  updateCollectionPage,
  updateCollectionActiveFilters,
} = collectionSlice.actions;

export const selectCollectionPageNumber: (
  payload: CollectionBasePayload
) => (state: RootState) => number = (payload) => (state) => {
  return state.collection[payload.id]?.page || 1;
};

export const selectCollectionNumberOfPages: (
  payload: CollectionBasePayload
) => (state: RootState) => number | undefined = (payload) => (state) => {
  if (
    !state.collection[payload.id]?.total ||
    !state.collection[payload.id]?.pageSize
  ) {
    return;
  }

  return Math.round(
    (state.collection[payload.id]?.total || 1) /
      (state.collection[payload.id]?.pageSize || 1)
  );
};

export const selectCollectionPage =
  <T extends Entity = Entity>(payload: CollectionGetPayload) =>
  (state: RootState) => {
    let collection: T[] | null = null;
    if (!state.collection[payload.id]) {
      return collection;
    }

    const page = payload.page || selectCollectionPageNumber(payload)(state);

    if (state.collection[payload.id]?.cache[page]) {
      collection = state.collection[payload.id]?.cache[page] as T[];
    }

    return collection;
  };

export const selectCollection =
  (payload: CollectionBasePayload) => (state: RootState) => {
    if (!state.collection[payload.id]) {
      return null;
    }

    return state.collection[payload.id] || null;
  };

export const selectCollectionFilters =
  (payload: CollectionBasePayload) => (state: RootState) => {
    if (!state.collection[payload.id]) {
      return null;
    }

    return state.collection[payload.id]!.filters || null;
  };

export const selectCollectionLoading: (
  payload: CollectionBasePayload
) => (state: RootState) => boolean = (payload) => (state) => {
  return (
    !state.collection[payload.id] ||
    state.collection[payload.id]?.status === STATUS_LOADING
  );
};

export const selectCollectionCount: (
  payload: CollectionBasePayload
) => (state: RootState) => number = (payload) => (state) => {
  if (!state.collection[payload.id]) {
    return 0;
  }

  return reduce(
    state.collection[payload.id]!.cache,
    (total, page) => {
      return total + (page?.length || 0);
    },
    0
  );
};

export const selectCollectionLastPage: (
  payload: CollectionBasePayload
) => (state: RootState) => boolean = (payload) => (state) => {
  return (
    !selectCollectionMorePagesAvailable(payload)(state) &&
    !selectCollectionPage({
      ...payload,
      page: selectCollectionPageNumber(payload)(state) + 1,
    })(state)
  );
};

export const selectCollectionMorePagesAvailable: (
  payload: CollectionBasePayload
) => (state: RootState) => boolean = (payload) => (state) => {
  if (!state.collection[payload.id]) {
    return false;
  }

  if (isNumber(state.collection[payload.id]!.total)) {
    return (
      selectCollectionCount(payload)(state) <
      state.collection[payload.id]!.total!
    );
  }

  return (
    state.collection[payload.id]!.cache[
      selectCollectionPageNumber(payload)(state)
    ]?.length === state.collection[payload.id]!.pageSize
  );
};

export default collectionSlice.reducer;
