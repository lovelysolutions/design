/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import api from "../../api";
import Entity from "./Entity";
import {
  validateOrReject,
  validateSync,
  ValidationError,
  ValidatorOptions,
} from "class-validator";
import { AxiosRequestConfig, AxiosResponse } from "axios";
import isNumber from "lodash/isNumber";
import omit from "lodash/omit";
import isNull from "lodash/isNull";
import isUndefined from "lodash/isUndefined";
import snakeCase from "lodash/snakeCase";
import isObject from "lodash/isObject";
import isString from "lodash/isString";

interface GetListData<T extends Model<Entity> = Model<Entity>> {
  data: T[];
  total?: number;
}

export interface FileUpload {
  file: File;
  key?: string;
}
export default class Model<T extends Entity = Entity> {
  // Constructor can be passed a partial entity or just the id of the entity
  constructor(slugIdOrPartial?: string | number | Partial<T> | null) {
    // TODO: try to cast string to number and use as id instead
    const base =
      (isString(slugIdOrPartial)
        ? { slug: slugIdOrPartial }
        : isNumber(slugIdOrPartial)
        ? { id: slugIdOrPartial }
        : slugIdOrPartial) || {};

    // Allow entity constructor to take additional actions
    this.entity = <T>(
      new (<typeof Model>this.constructor).entityConstructor(base)
    );

    Object.assign(this.entity, base, this.entity);
  }

  // Redefined in subclasses

  // API url
  static url: string;

  // A copy of the entity constructor so that we can call new on it in here
  static entityConstructor: new (...args: any[]) => Entity;

  // Things needed for this class

  // The model/entity that we take actions on
  entity: T;

  // Used to wait until finished for certain methods
  private savingPromise: Promise<AxiosResponse<any>> | undefined;

  // Shorcut to the model id
  get id() {
    return this.entity.id;
  }

  // Shorcut to the model slug
  get slug() {
    return this.entity.slug;
  }

  equals(model: Model<T>) {
    return this.id ? model.id === this.id : this === model;
  }

  async upload(
    uploads: FileUpload[],
    axiosConfig?: AxiosRequestConfig,
    validatorOptions?: ValidatorOptions
  ) {
    const formData = new FormData();

    // formData.append("authentication_token", Authentication.token());

    if (this.id) {
      formData.append("id", String(this.id));
    }

    for (const upload of uploads) {
      formData.append(
        `data[${upload.key || `file${uploads.length > 1 ? "[]" : ""}`}]`,
        upload.file
      );
    }

    const serialized = this.serialize();

    for (const key in serialized) {
      if (!isNull(serialized[key]) && !isUndefined(serialized[key])) {
        formData.append(
          `data[${snakeCase(key)}]`,
          isObject(serialized[key])
            ? JSON.stringify(serialized[key])
            : serialized[key]
        );
      }
    }

    return this.save(
      {
        ...axiosConfig,
        data: formData,
      },
      validatorOptions
    );
  }

  // PUT or POST method
  async save(
    axiosConfig?: AxiosRequestConfig,
    validatorOptions?: ValidatorOptions
  ) {
    // If not saved to server yet, wait for the first save before trying again
    if (!this.id && this.savingPromise) {
      await this.savingPromise;
    }

    await this.validate(validatorOptions);

    // Call the method with the URL, data, and axios config
    const promise = api({
      url: this.constructUrl(),
      // If there's an id, call the PUT method, otherwise the POST
      method: this.entity.id ? "put" : "post",
      data: this.serialize(),
      ...axiosConfig,
    });

    this.savingPromise = promise;

    // Axios response
    const { data } = await promise;

    // Set the id on the model if not set and was returned
    if (!this.id && data.id) {
      this.entity.id = data.id;
    }

    return this;
  }

  // GET method
  async get(axiosConfig?: AxiosRequestConfig) {
    if (!this.id && !this.slug) {
      throw new Error("Id or slug must be present to GET model");
    }

    const { data } = await api.get(this.constructUrl(), axiosConfig);

    // Clone of constructor functionality in case entity constructor takes actions
    this.entity = <T>(
      new (<typeof Model>this.constructor).entityConstructor(data)
    );

    Object.assign(this.entity, data, this.entity);

    return this;
  }

  // DELETE method
  async delete(axiosConfig?: AxiosRequestConfig) {
    if (!this.id) {
      return;
    }

    await api.delete(this.constructUrl(), axiosConfig);

    return this;
  }

  // Turn model into raw data to send to API
  serialize(): any {
    const { ...object } = this.entity;
    // TODO: do we pass metadata anywhere?
    return omit(object, "createdAt", "updatedAt", "metadata");
  }

  // URL with id in it or not
  constructUrl() {
    const idOrSlug = this.id || this.slug;

    return idOrSlug
      ? `${(<typeof Model>this.constructor).url}/${idOrSlug}`
      : (<typeof Model>this.constructor).url;
  }

  get valid() {
    return !validateSync(this.entity as T).length;
  }
  // Validate unserialized model
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async validate(options?: ValidatorOptions, _key?: keyof T) {
    return validateOrReject(this.entity as T, options);
  }

  async validateProperty(key: keyof T, options?: ValidatorOptions) {
    try {
      await this.validate(options, key);
    } catch (errors: unknown) {
      if ((errors as ValidationError[])?.length) {
        const relevantErrors = (errors as ValidationError[]).filter(
          (error) => error.property === key
        );
        if (relevantErrors.length) {
          throw relevantErrors;
        }
      }
    }
  }

  // GET a list of this model. Static because there is no instance to start with. Returns array of instances and metadata
  static async getList<T extends Model<Entity>>(
    axiosConfig?: AxiosRequestConfig
  ): Promise<GetListData<T>> {
    const { data } = await api.get(this.url, axiosConfig);

    if (!Array.isArray(data.data)) {
      throw new Error("Data not returned in the form of an array");
    }

    return {
      ...data,
      data: data.data.map((entity: Entity) => {
        return new this(entity);
      }),
    };
  }
}
