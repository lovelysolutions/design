/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import { useEffect, useRef, useState } from "react";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import {
  selectEntity,
  BasePayload,
  reset,
  entityFetch,
  updateEntityDraft,
  deleteEntity,
  setEntityStatus,
} from "./entitySlice";
import {
  selectEntityDraft,
  selectEntityLoading,
  saveEntityDraft,
  setEntity,
} from "./entitySlice";
import Model from "./Model";
import Entity from "./Entity";
import { RootState } from "../../app/store";
import { ThunkDispatch, AnyAction } from "@reduxjs/toolkit";
import { selectEntityFailed, selectEntityErrors } from "./entitySlice";

type UseModelReturnProps<
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity
> = {
  model: T;
  onChange: (entity: P) => Promise<any>;
  onSubmit: () => void;
  loading?: boolean;
  initializing?: boolean;
  submitted: boolean;
  failed: boolean;
  resetState: () => void;
  current: P | null;
  draft: P | null;
  onDelete: () => Promise<void>;
  errors?: string[];
};

export type UseModelProps<
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity
> = {
  idOrSlug: number | string | undefined;
  payload: BasePayload;
  ModelType: new (...args: any[]) => T;
  instantiate?: () => P | Promise<P>;
  canSave?: boolean;
  canDelete?: boolean;
  cache?: boolean;
  upload?: (draft: P, current: P) => Promise<any>;
  save?: (draft: P, current: P) => Promise<any>;
  fetch?: (
    state: RootState,
    dispatch: ThunkDispatch<unknown, unknown, AnyAction>
  ) => Promise<Entity | null>;
  loading?: boolean;
  afterSave?: (model: T) => any | Promise<any>;
  afterDelete?: (model: T) => any | Promise<any>;
};

const useModel = <
  T extends Model<Entity> = Model<Entity>,
  P extends Entity = Entity
>({
  fetch,
  idOrSlug,
  payload,
  ModelType,
  instantiate,
  canSave,
  canDelete,
  cache,
  upload,
  save,
  loading,
  afterSave,
  afterDelete,
}: UseModelProps<T, P>) => {
  const mounted = useRef(false);

  const dispatch = useAppDispatch();

  const [submitted, setSubmitted] = useState<boolean>(false);
  const [inTransition, setInTransition] = useState<boolean>(false);

  const draft = useAppSelector(selectEntityDraft<P>(payload));
  const current = useAppSelector(selectEntity<P>(payload));
  const entityLoading = useAppSelector(selectEntityLoading(payload));
  const failed = useAppSelector(selectEntityFailed(payload));
  const errors = useAppSelector(selectEntityErrors(payload));

  const model = new ModelType(draft || current || idOrSlug);

  const initializing = !current || (canSave && !draft);

  const resetState = async () => {
    setSubmitted(false);
    await dispatch(setEntityStatus(payload));
  };

  const onChange = async (updatedEntity: Entity) => {
    await resetState();
    return dispatch(updateEntityDraft({ ...payload, entity: updatedEntity }));
  };

  useEffect(() => {
    if (inTransition || failed || entityLoading || !submitted) {
      return;
    }

    const onSubmitted = async () => {
      setInTransition(true);
      await onSave();
    };

    onSubmitted();
  }, [submitted, entityLoading, failed, inTransition]);

  const onSubmit = async () => {
    await dispatch(setEntityStatus(payload));
    setSubmitted(true);
  };

  const onSave = async () => {
    if (!canSave) {
      return;
    }

    const results = await dispatch(
      upload
        ? saveEntityDraft({
            payload,
            save: upload as (draft: Entity, current: Entity) => Promise<any>,
          })
        : save
        ? saveEntityDraft({
            payload,
            save: save as (draft: Entity, current: Entity) => Promise<any>,
            regardless: true,
          })
        : saveEntityDraft({ payload, model })
    );

    if (!mounted.current) {
      return;
    }

    // TODO: Set a failure flag/message?
    if ((results as any).error) {
      setInTransition(false);
      return;
    }

    setSubmitted(false);

    afterSave?.(model);
  };

  const onDelete = async () => {
    if (!canDelete) {
      return;
    }

    setSubmitted(false);

    const results = await dispatch(deleteEntity({ payload, model }));

    // TODO: Set a failure flag/message?
    if ((results as any).error) {
      return;
    }

    afterDelete?.(model);
  };

  useEffect(() => {
    mounted.current = true;

    return () => {
      mounted.current = false;
      if (cache) {
        return;
      }
      dispatch(reset(payload));
    };
  }, []);

  useEffect(() => {
    const idChanged = async () => {
      if (!mounted.current || entityLoading || loading) {
        return;
      }

      if (
        current &&
        ((!current.id && !current.slug && !idOrSlug) ||
          (!!idOrSlug && current.id && +current.id === +idOrSlug) ||
          (current.slug && current.slug === idOrSlug))
      ) {
        return;
      }

      if (current) {
        await dispatch(reset(payload));
      }

      if (!idOrSlug) {
        if (instantiate) {
          dispatch(setEntity({ ...payload, entity: await instantiate() }));
        } else {
          // TODO: set an error?
        }
        return;
      }

      const fetchAction = fetch ? { payload, fetch } : { payload, model };
      const results = await dispatch(entityFetch(fetchAction));

      if ((results as any).error) {
        // TODO: set an error?
      }
    };

    idChanged();

    setSubmitted(false);
    setInTransition(false);

    return () => {
      if (cache) {
        return;
      }
      dispatch(reset(payload));
    };
  }, [idOrSlug, loading]);

  useEffect(() => {
    if (!current || inTransition || !canSave || loading) {
      return;
    }

    dispatch(
      updateEntityDraft({
        ...payload,
        entity: {
          ...draft,
          ...current,
        },
      })
    );
  }, [current, loading]);

  const returnProps: UseModelReturnProps<T, P> = {
    model: model,
    onChange: onChange,
    initializing,
    loading: entityLoading || initializing,
    submitted: submitted,
    resetState,
    onSubmit,
    current,
    draft,
    onDelete,
    errors,
    failed,
  };

  return returnProps;
};

export default useModel;
