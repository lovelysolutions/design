/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import { IsOptional, IsInt } from "class-validator";

export default class Entity {
  @IsOptional()
  @IsInt()
  id?: number;

  slug?: string;

  metadata?: {
    [key: string]: any;
  };

  createdAt?: string;
  updatedAt?: string;
}
