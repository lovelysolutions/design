/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  createAsyncThunk,
  createSlice,
  PayloadAction,
  ThunkDispatch,
  AnyAction,
} from "@reduxjs/toolkit";
import { AxiosError } from "axios";
import isEqual from "lodash/isEqual";
import { RootState } from "../../app/store";
import Entity from "./Entity";
import Model from "./Model";

type Status = "loading" | "failed" | "deleted";
export const STATUS_LOADING = "loading";
export const STATUS_FAILED = "failed";
export const STATUS_DELETED = "deleted";

interface EntityCache {
  [modelType: string]: {
    [entityId: string]: EntityState | undefined;
  };
}

const initialState: EntityCache = {};

interface ResetPayload {
  type: string;
}

export type BasePayload = ResetPayload & {
  id?: string;
};

type FetchActionBase = {
  payload: BasePayload;
};

type FetchActionFetch = FetchActionBase & {
  fetch: (
    state: RootState,
    dispatch: ThunkDispatch<unknown, unknown, AnyAction>
  ) => Promise<Entity | null>;
};

type FetchActionModel = FetchActionBase & {
  model: Model<Entity>;
};

export type FetchAction = FetchActionFetch | FetchActionModel;

interface SaveActionBase {
  payload: BasePayload;
  regardless?: boolean;
}

type SaveActionSave = SaveActionBase & {
  save: (draft: Entity, current: Entity) => Promise<any>;
};

type SaveActionClass = SaveActionBase & {
  modelClass: new (...args: any[]) => Model<Entity>;
};

type SaveActionModel = SaveActionBase & {
  model: Model<Entity>;
};

export type SaveAction = SaveActionSave | SaveActionClass | SaveActionModel;

type DeleteActionBase = {
  payload: BasePayload;
};

type DeleteActionDelete = DeleteActionBase & {
  delete: () => Promise<any>;
};

type DeleteActionModel = DeleteActionBase & {
  model: Model<Entity>;
};

export type DeleteAction = DeleteActionDelete | DeleteActionModel;

export type UpdatePayload = BasePayload & {
  entity: Entity | null;
};

export type StatusUpdate = BasePayload & {
  status?: Status;
  errors?: string[];
};

export interface EntityState {
  current: Entity | null;
  draft: Entity | null;
  status?: Status;
  errors?: string[];
}

const initialEntityState: EntityState = {
  current: null,
  draft: null,
};

export const entityFetch = createAsyncThunk(
  "entity/fetch",
  async (action: FetchAction, { dispatch, getState }) => {
    dispatch(setEntityStatus({ ...action.payload, status: STATUS_LOADING }));

    try {
      let entity: Entity | null = {};
      if ((action as FetchActionFetch).fetch) {
        entity = await (action as FetchActionFetch).fetch(
          getState() as RootState,
          dispatch
        );
      }
      if ((action as FetchActionModel).model) {
        entity = { ...(await (action as FetchActionModel).model.get()).entity };
      }
      dispatch(setEntity({ ...action.payload, entity }));
      dispatch(setEntityStatus(action.payload));
    } catch (error) {
      dispatch(setEntityStatus({ ...action.payload, status: STATUS_FAILED }));
      throw error;
    }
  }
);

export const saveEntityDraft = createAsyncThunk(
  "entity/saveDraft",
  async (action: SaveAction, { getState, dispatch }) => {
    const draft = selectEntityDraft(action.payload)(getState() as RootState);
    if (!draft) {
      return;
    }

    const current = selectEntity(action.payload)(getState() as RootState);

    if (!current) {
      return;
    }

    if (!action.regardless && draft.id && isEqual(draft, current)) {
      await dispatch(commitEntityDraft(action.payload));
      return;
    }

    await dispatch(
      setEntityStatus({ ...action.payload, status: STATUS_LOADING })
    );

    try {
      if ((action as SaveActionSave).save) {
        await (action as SaveActionSave).save(draft, current);
      } else if ((action as SaveActionClass).modelClass) {
        const model = new (action as SaveActionClass).modelClass(draft);
        await model.save();
      } else if ((action as SaveActionModel).model) {
        await (action as SaveActionModel).model.save();
      }

      await dispatch(setEntityStatus(action.payload));
      await dispatch(commitEntityDraft(action.payload));
    } catch (error) {
      let errors: string[] | undefined = (error as AxiosError | undefined)
        ?.response?.data?.errors;

      if (
        !errors?.length &&
        (error as AxiosError | undefined)?.response?.data?.message
      ) {
        errors = [(error as AxiosError | undefined)?.response?.data?.message];
      }

      await dispatch(
        setEntityStatus({ ...action.payload, status: STATUS_FAILED, errors })
      );

      throw error;
    }
  }
);

export const deleteEntity = createAsyncThunk(
  "entity/delete",
  async (action: DeleteAction, { dispatch }) => {
    await dispatch(
      setEntityStatus({ ...action.payload, status: STATUS_LOADING })
    );

    try {
      if ((action as DeleteActionDelete).delete) {
        await (action as DeleteActionDelete).delete();
      }
      if ((action as DeleteActionModel).model) {
        await (action as DeleteActionModel).model.delete();
      }
      await dispatch(
        setEntityStatus({ ...action.payload, status: STATUS_DELETED })
      );
      await dispatch(reset(action.payload));
    } catch (error) {
      await dispatch(
        setEntityStatus({ ...action.payload, status: STATUS_FAILED })
      );
    }
  }
);

const entityId: (payload: BasePayload) => string = (payload) =>
  payload.id ? payload.id : "new";

const ensureEntityState = (state: EntityCache, payload: BasePayload) => {
  if (!state[payload.type]) {
    state[payload.type] = {};
  }

  if (!state[payload.type][entityId(payload)]) {
    state[payload.type][entityId(payload)] = {
      ...initialEntityState,
    };
  }
};

export const entitySlice = createSlice({
  name: "entity",
  initialState,
  reducers: {
    resetEntities: () => ({ ...initialState }),
    resetAll: (state, action: PayloadAction<BasePayload>) => {
      Object.assign(state, {
        ...state,
        [action.payload.type]: {},
      });
    },
    reset: (state, action: PayloadAction<BasePayload>) => {
      Object.assign(state, {
        ...state,
        [action.payload.type]: {
          ...state[action.payload.type],
          [entityId(action.payload)]: undefined,
        },
      });
    },
    setEntityLoading: (state, action: PayloadAction<BasePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.status =
        STATUS_LOADING;
    },
    setEntityDoneLoading: (state, action: PayloadAction<BasePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.status = undefined;
    },
    setEntityFailed: (state, action: PayloadAction<BasePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.status =
        STATUS_FAILED;
    },
    setEntityStatus: (state, action: PayloadAction<StatusUpdate>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.status =
        action.payload.status;
      state[action.payload.type][entityId(action.payload)]!.errors =
        action.payload.errors;
    },
    setEntity: (state, action: PayloadAction<UpdatePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.current =
        action.payload.entity;
    },
    updateEntityDraft: (state, action: PayloadAction<UpdatePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.draft =
        action.payload.entity;
    },
    commitEntityDraft: (state, action: PayloadAction<BasePayload>) => {
      ensureEntityState(state, action.payload);

      state[action.payload.type][entityId(action.payload)]!.current =
        state[action.payload.type][entityId(action.payload)]!.draft;

      state[action.payload.type][entityId(action.payload)]!.draft = null;
    },
  },
  // extraReducers: (builder) => {},
});

export const {
  resetEntities,
  resetAll,
  reset,
  setEntityStatus,
  setEntityLoading,
  setEntityFailed,
  setEntityDoneLoading,
  setEntity,
  updateEntityDraft,
  commitEntityDraft,
} = entitySlice.actions;

export const selectEntity: <T extends Entity = Entity>(
  payload: BasePayload
) => (state: RootState) => T | null =
  <T extends Entity = Entity>(payload: BasePayload) =>
  (state: RootState) =>
    (state.entity[payload.type]?.[entityId(payload)]?.current as
      | T
      | undefined) || null;

export const selectEntityDraft: <T extends Entity = Entity>(
  payload: BasePayload
) => (state: RootState) => T | null =
  <T extends Entity = Entity>(payload: BasePayload) =>
  (state: RootState) =>
    (state.entity[payload.type]?.[entityId(payload)]?.draft as T | undefined) ||
    null;

export const selectEntityLoading: (
  payload: BasePayload
) => (state: RootState) => boolean = (payload) => (state) =>
  state.entity[payload.type]?.[entityId(payload)]?.status === STATUS_LOADING;

export const selectEntityDeleted: (
  payload: BasePayload
) => (state: RootState) => boolean = (payload) => (state) =>
  state.entity[payload.type]?.[entityId(payload)]?.status === STATUS_DELETED;

export const selectEntityFailed: (
  payload: BasePayload
) => (state: RootState) => boolean = (payload) => (state) =>
  state.entity[payload.type]?.[entityId(payload)]?.status === STATUS_FAILED;

export const selectEntityErrors: (
  payload: BasePayload
) => (state: RootState) => string[] | undefined = (payload) => (state) =>
  state.entity[payload.type]?.[entityId(payload)]?.errors;

export default entitySlice.reducer;
