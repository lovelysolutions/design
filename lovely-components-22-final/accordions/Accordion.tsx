/**
 * Lovely Design System Components
 * @author Lovely Solutions LLC
 * @link https://gitlab.com/lovelysolutions/design
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

import React, {
  forwardRef,
  MouseEventHandler,
  useState,
  ReactNode,
  useEffect,
  useRef,
} from "react";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import Button from "../buttons/Button";

export type AccordionProps = React.HTMLProps<HTMLDivElement> & {
  title: string;
  startOpen?: boolean;
  openIcon?: IconDefinition;
  closedIcon?: IconDefinition;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  buttonContent?: ReactNode;
};

const Accordion = forwardRef<HTMLDivElement, AccordionProps>(
  (
    {
      children,
      className,
      startOpen = false,
      title,
      onClick,
      buttonContent,
      ...rest
    },
    ref
  ) => {
    const mounted = useRef(false);

    const [open, setOpen] = useState<boolean>(startOpen);

    const onButtonClick = onClick ? onClick : () => setOpen(!open);

    useEffect(() => {
      mounted.current = true;
      return () => {
        mounted.current = false;
      };
    }, []);

    useEffect(() => {
      if (startOpen && mounted.current) {
        setOpen(true);
      }
    }, [startOpen]);

    return (
      <div {...rest} ref={ref}>
        <Button
          onClick={onButtonClick}
          variant="unstyled"
          className={classNames(
            "flex flex-nowrap items-center pl-4 pr-1 w-full text-left",
            className ||
              "font-medium bg-backgroundAccordion hover:bg-backgroundAccordionHover cursor-pointer rounded-md shadow-sm"
          )}
        >
          {buttonContent ? (
            buttonContent
          ) : (
            <div className="text-sm flex-grow">{title}</div>
          )}
          <div className="block p-3 text-gray-400 hover:text-textOrange">
            <FontAwesomeIcon icon={open ? faCaretUp : faCaretDown} />
          </div>
        </Button>
        <div
          className={classNames(
            // TODO: animation not great
            "overflow-hidden transform origin-top transition-transform",
            open ? "scale-y-1" : "scale-y-0 max-h-0"
          )}
        >
          {children}
        </div>
      </div>
    );
  }
);

Accordion.displayName = "Accordion";

export default Accordion;
