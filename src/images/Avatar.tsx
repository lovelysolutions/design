import React, { forwardRef } from "react";
import Image from "./Image";

export type AvatarProps = Omit<
  React.HTMLProps<HTMLImageElement>,
  "crossOrigin"
> & {
  //
};

const Avatar = forwardRef<HTMLImageElement, AvatarProps>(({ ...rest }, ref) => {
  return (
    <div className="w-24 h-24 rounded-full overflow-hidden flex items-stretch bg-gray-200 border border-gray-200">
      <Image {...rest} ref={ref} />
    </div>
  );
});

Avatar.displayName = "Avatar";

export default Avatar;
