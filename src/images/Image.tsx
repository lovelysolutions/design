import React, { forwardRef, ReactNode, useState } from "react";
import classNames from "classnames";

export type ImageProps = Omit<
  React.HTMLProps<HTMLImageElement>,
  "crossOrigin"
> & {
  backupSrc?: string;
  backup?: ReactNode;
  fit?: "contain" | "cover" | "fill";
};

const TRANSPARENT_PIXEL =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";

const Image = forwardRef<HTMLImageElement, ImageProps>(
  ({ className, src, backupSrc, backup, fit = "cover", ...rest }, ref) => {
    const [loaded, setLoaded] = useState<boolean>(false);
    const [failed, setFailed] = useState<boolean>(false);

    const failedSrc = backupSrc || TRANSPARENT_PIXEL;

    return (
      <div
        className={classNames(
          "w-full h-full",
          className,
          (failed || !loaded) && "bg-gray-100"
        )}
      >
        {failed && backup ? (
          <span className="animate-fade-in">{backup}</span>
        ) : (
          <img
            {...rest}
            ref={ref}
            onLoad={() => setLoaded(true)}
            className={classNames(
              !loaded && !failed ? "opacity-0" : "opacity-100",
              "w-full h-full before:hidden transition-opacity",
              `object-${fit}`
              // object-cover - tailwind not generating within storybook without this?
            )}
            src={failed ? failedSrc : src}
            onError={() => {
              setLoaded(false);
              setFailed(true);
            }}
          />
        )}
      </div>
    );
  }
);

Image.displayName = "Image";

export default Image;
