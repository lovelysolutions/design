import React, { forwardRef } from "react";
import { Switch as Toggle } from "@headlessui/react";
import omit from "lodash/omit";
import classNames from "classnames";

type SwitchProps = Omit<
  React.HTMLProps<HTMLButtonElement>,
  "onChange" | "value"
> & {
  onChange: (value: boolean) => void;
  value?: boolean;
};

const Switch = forwardRef<HTMLButtonElement, SwitchProps>(
  ({ className, children, value, onChange, ...rest }, ref) => {
    return (
      <Toggle.Group>
        <div className={classNames("flex items-center", className)}>
          {!!children && (
            <Toggle.Label className="mr-4">{children}</Toggle.Label>
          )}
          <Toggle
            {...omit(rest, "as", "type")}
            onChange={onChange}
            checked={!!value}
            className={classNames(
              value ? "bg-magenta-5" : "bg-gray-200",
              "relative inline-flex items-center h-6 rounded-full w-11 transition-colors disabled:opacity-60 disabled:cursor-default focus:outline-none focus:ring-2 focus:ring-offset-white ring-offset-1 focus:ring-magenta-5"
            )}
            ref={ref}
            type="button"
          >
            <span
              className={`${
                value ? "translate-x-6" : "translate-x-1"
              } inline-block w-4 h-4 transform bg-white rounded-full transition-transform`}
            />
          </Toggle>
        </div>
      </Toggle.Group>
    );
  }
);

Switch.displayName = "Switch";

export default Switch;
