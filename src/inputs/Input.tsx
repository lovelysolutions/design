// https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#autofill
// https://developers.google.com/web/fundamentals/design-and-ux/input/forms

import classNames from "classnames";
import React, { ForwardedRef, forwardRef } from "react";
import isFunction from "lodash/isFunction";

import "./input.css";

interface SelectOption {
  value: any;
  name?: string;
}

export type InputProps = Omit<
  React.HTMLProps<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>,
  "type"
> & {
  type?: InputType;
  autoCompleteProps?: any;
  options?: SelectOption[];
};

interface InputTypeDefinition {
  name: string;
  type?: string;
  autocomplete?: ((props: any) => string) | string;
  pattern?: string;
}

interface InputTypeDefinitionMap {
  [type: string]: InputTypeDefinition;
}

// https://developers.google.com/web/fundamentals/design-and-ux/input/forms

const REG_EX_POSTAL_ADDRESS = /[a-zA-Z\d\s\-,#.+]+/;
const REG_EX_ZIP_CODE = /^\d{5,6}(?:[-\s]\d{4})?$/;
const REG_EX_CREDIT_CARD_NUMBER =
  /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$/;
const REG_EX_SOCIAL_SECURITY = /^\d{3}-\d{2}-\d{4}$/;
const REG_EX_PHONE_NUMBER_NORTH_AMERICA =
  /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

export const INPUT_TYPES: InputType[] = [
  "text",
  "textarea",
  "select",
  "checkbox",
  "radio",
  "number",
  "email",
  "password",
  "username",
  "url",
  "search",
  "range",
  "datetime-local",
  "date",
  "time",
  "week",
  "month",
  "color",
  "name",
  "fname",
  "mname",
  "lname",
  "address",
  "city",
  "region",
  "province",
  "state",
  "zip",
  "zip2",
  "postal",
  "country",
  "phone",
  "mobile",
  "country-code",
  "area-code",
  "exchange",
  "suffix",
  "ext",
  "ccname",
  "cardnumber",
  "cvc",
  "ccmonth",
  "ccyear",
  "exp-date",
  "card-type",
  "ssn",
  // "honorific-prefix",
  // "honorific-suffix",
];

export type InputType =
  | "text"
  | "select"
  | "checkbox"
  | "radio"
  | "textarea"
  | "number"
  | "email"
  | "password"
  | "username"
  | "url"
  | "search"
  | "range"
  | "datetime-local"
  | "date"
  | "time"
  | "week"
  | "month"
  | "color"
  | "name"
  | "fname"
  | "mname"
  | "lname"
  | "address"
  | "city"
  | "region"
  | "province"
  | "state"
  | "zip"
  | "zip2"
  | "postal"
  | "country"
  | "phone"
  | "mobile"
  | "country-code"
  | "area-code"
  | "exchange"
  | "suffix"
  | "ext"
  | "ccname"
  | "cardnumber"
  | "cvc"
  | "ccmonth"
  | "ccyear"
  | "exp-date"
  | "card-type"
  | "ssn";

export const INPUT_TYPE_MAP: InputTypeDefinitionMap = {
  radio: {
    name: "radio",
    type: "radio",
  },
  checkbox: {
    name: "checkbox",
    type: "checkbox",
  },
  ssn: {
    name: "ssn",
    pattern: REG_EX_SOCIAL_SECURITY.source,
  },
  "datetime-local": {
    name: "datetime-local",
    type: "datetime-local",
  },
  text: {
    name: "text",
    type: "text",
  },
  color: {
    name: "color",
    type: "color",
  },
  month: {
    name: "month",
    type: "month",
  },
  week: {
    name: "week",
    type: "week",
  },
  time: {
    name: "time",
    type: "time",
  },
  date: {
    name: "date",
    type: "date",
  },
  range: {
    name: "range",
    type: "range",
  },
  number: {
    name: "number",
    type: "number",
  },
  search: {
    name: "search",
    type: "search",
  },
  url: {
    name: "url",
    type: "url",
  },
  name: {
    name: "name",
    autocomplete: "name",
  },
  fname: {
    name: "fname",
    autocomplete: "given-name",
  },
  mname: {
    name: "mname",
    autocomplete: "additional-name",
  },
  lname: {
    name: "lname",
    autocomplete: "family-name",
  },
  email: {
    name: "email",
    type: "email",
    autocomplete: "email",
  },
  address: {
    name: "address",
    pattern: REG_EX_POSTAL_ADDRESS.source,
    autocomplete: ({ lineNumber }) =>
      lineNumber ? `address-line${lineNumber}` : "street-address",
  },
  city: {
    name: "city",
    autocomplete: "address-level2",
  },
  region: {
    name: "region",
  },
  province: {
    name: "province",
    autocomplete: "address-level1",
  },
  state: {
    name: "state",
    autocomplete: "address-level1",
  },
  zip: {
    name: "zip",
    autocomplete: "postal-code",
    pattern: REG_EX_ZIP_CODE.source,
  },
  zip2: {
    name: "zip2",
  },
  postal: {
    name: "postal",
    autocomplete: "postal-code",
    pattern: REG_EX_ZIP_CODE.source,
  },
  country: {
    name: "country",
    autocomplete: "country",
  },
  phone: {
    name: "phone",
    autocomplete: "tel",
    type: "tel",
    // TODO: support international numbers
    pattern: REG_EX_PHONE_NUMBER_NORTH_AMERICA.source,
  },
  mobile: {
    name: "mobile",
    autocomplete: "tel",
    type: "tel",
    // TODO: support international numbers
    pattern: REG_EX_PHONE_NUMBER_NORTH_AMERICA.source,
  },
  "country-code": {
    name: "country-code",
  },
  "area-code": {
    name: "area-code",
  },
  exchange: {
    name: "exchange",
  },
  suffix: {
    name: "suffix",
  },
  ext: {
    name: "ext",
  },
  ccname: {
    name: "ccname",
    autocomplete: "cc-name",
  },
  cardnumber: {
    name: "cardnumber",
    autocomplete: "cc-number",
    pattern: REG_EX_CREDIT_CARD_NUMBER.source,
  },
  cvc: {
    name: "cvc",
    autocomplete: "cc-csc",
  },
  ccmonth: {
    name: "ccmonth",
    autocomplete: "cc-exp-month",
  },
  ccyear: {
    name: "ccyear",
    autocomplete: "cc-exp-year",
  },
  "exp-date": {
    name: "exp-date",
    autocomplete: "cc-exp",
  },
  "card-type": {
    name: "card-type",
    autocomplete: "cc-type",
  },
  username: {
    name: "username",
    autocomplete: "username",
  },
  password: {
    name: "password",
    type: "password",
    autocomplete: ({ newPassword }) =>
      newPassword ? "new-password" : "current-password",
  },
};

interface InputTypeClassMap {
  [type: string]: string;
}

const INPUT_TYPE_CLASS_MAP: InputTypeClassMap = {
  input: "bg-transparent",
  select: "appearance-none",
  radio:
    "bg-white hover:bg-gray-50 appearance-none rounded-full w-4 h-4 border-2 border-magenta-5 checked:border-[0.35rem] cursor-pointer hover:border-magenta-6 transition-all",
  checkbox:
    "bg-white grid place-content-center appearance-none w-4 h-4 border-2 border-magenta-5 checked:border-[0.5rem] cursor-pointer hover:border-magenta-6 transition-all before:bg-white before:content-[''] before:w-2 before:h-2 before:transition-all rounded",
  shared:
    "disabled:opacity-60 focus:outline-none focus:ring-2 focus:ring-offset-white ring-offset-1 focus:ring-magenta-5",
};

const DEFAULT_INPUT_TYPE_CLASS_MAP: InputTypeClassMap = {
  input: "rounded px-1 py-0.5 border flex-grow",
  radio: " ", // TODO: check if defined instead?
  checkbox: " ", // TODO: check if defined instead?
};

const Input = forwardRef<
  HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement,
  InputProps
>(
  (
    {
      type = "text",
      className,
      noValidate,
      autoComplete,
      autoCompleteProps = {},
      children,
      options,
      name,
      id,
      ...rest
    },
    ref
  ) => {
    const typeDefinition: InputTypeDefinition =
      INPUT_TYPE_MAP[type] || INPUT_TYPE_MAP["text"];

    const definedAutoCompleteString =
      (isFunction(typeDefinition.autocomplete)
        ? typeDefinition.autocomplete(autoCompleteProps)
        : typeDefinition.autocomplete) || "off";

    const fullAutoCompleteString = autoComplete
      ? `${autoComplete} ${definedAutoCompleteString}}`
      : definedAutoCompleteString;

    const props = {
      ...rest,
      name,
      id,
      className: classNames(
        INPUT_TYPE_CLASS_MAP[type] || INPUT_TYPE_CLASS_MAP["input"],
        className ||
          DEFAULT_INPUT_TYPE_CLASS_MAP[type] ||
          DEFAULT_INPUT_TYPE_CLASS_MAP["input"],
        INPUT_TYPE_CLASS_MAP["shared"]
      ),
      type: typeDefinition?.type || "text",
      ref,
      autoComplete: fullAutoCompleteString,
      pattern: typeDefinition.pattern,
      noValidate,
    };

    // TODO: return 2 inputs for 2 line address? Move to field

    let input = (
      <input {...props} ref={ref as ForwardedRef<HTMLInputElement>} />
    );

    if (type === "select") {
      input = (
        <select {...props} ref={ref as ForwardedRef<HTMLSelectElement>}>
          {(options || []).map((option, optionIndex) => (
            <option key={`${name || id}-${optionIndex}`} value={option.value}>
              {option.name || option.value}
            </option>
          ))}
        </select>
      );
    }

    if (type === "textarea") {
      input = (
        <textarea {...props} ref={ref as ForwardedRef<HTMLTextAreaElement>} />
      );
    }

    return (
      <div className="flex space-x-2 items-center flex-grow">
        {input}
        {children}
      </div>
    );
  }
);

Input.displayName = "Input";

export default Input;
