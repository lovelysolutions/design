import React, { forwardRef } from "react";
import classNames from "classnames";

interface VariantConfiguration {
  classes: string;
  // Override other variants?
  variants?: {
    [variant: string]: VariantConfigurationMap;
  };
}

interface VariantConfigurationMap {
  [variant: string]: VariantConfiguration | undefined;
}

const SIZE_VARIANT_MAP_PADDED_BUTTON: VariantConfigurationMap = {
  xs: { classes: "px-1.5 py-1" },
  sm: { classes: "px-2 py-1" },
  md: { classes: "px-2.5 py-1.5" },
  lg: { classes: "px-3 py-2" },
  xl: { classes: "px-4 py-3" },
};

const SIZE_VARIANT_MAP_UNPADDED_BUTTON: VariantConfigurationMap = {
  xs: { classes: "py-1" },
  sm: { classes: "py-1" },
  md: { classes: "py-1.5" },
  lg: { classes: "py-2" },
  xl: { classes: "py-3" },
};

type ThemeVariant =
  | "transparent"
  | "link"
  | "primary"
  | "secondary"
  | "light"
  | "dark";

const ThemeClasses: VariantConfigurationMap = {
  transparent: {
    classes: "", // "text-magenta-5 hover:text-magenta-4",
    variants: {
      size: SIZE_VARIANT_MAP_UNPADDED_BUTTON,
    },
  },
  link: {
    classes: "text-blue-500 hover:text-blue-600",
    variants: {
      rounded: {
        override: { classes: "rounded-none" },
      },
      size: SIZE_VARIANT_MAP_UNPADDED_BUTTON,
    },
  },
  primary: {
    classes: "text-white bg-magenta-5 hover:bg-magenta-4",
    variants: {
      size: SIZE_VARIANT_MAP_PADDED_BUTTON,
      rounded: {
        default: { classes: "rounded" },
      },
    },
  },
  secondary: {
    classes:
      "text-magenta-5 bg-magenta-11 hover:bg-magenta-10 hover:text-magenta-4",
    variants: {
      size: SIZE_VARIANT_MAP_PADDED_BUTTON,
      border: {
        override: { classes: "border border-magenta-5" },
      },
      rounded: {
        default: { classes: "rounded" },
      },
    },
  },
  light: {
    classes: "text-black bg-light hover:bg-gray-50 hover:text-gray-800",
    variants: {
      size: SIZE_VARIANT_MAP_PADDED_BUTTON,
      border: {
        override: { classes: "border border-gray-300" },
      },
      rounded: {
        default: { classes: "rounded" },
      },
    },
  },
  dark: {
    classes: "text-white bg-black hover:bg-gray-700 hover:text-gray-50",
    variants: {
      size: SIZE_VARIANT_MAP_PADDED_BUTTON,
      // border: {
      //   override: { classes: "border border-gray-300" },
      // },
      rounded: {
        default: { classes: "rounded" },
      },
    },
  },
};

type BorderVariant = "border" | "none";
const BorderClasses: VariantConfigurationMap = {
  none: {
    classes: "border border-transparent",
  },
  border: {
    classes: "border",
  },
};
type RoundedVariant = "rounded" | "none" | "circle";
const RoundedClasses: VariantConfigurationMap = {
  none: {
    classes: "rounded-none",
  },
  circle: {
    classes: "rounded-full",
    variants: {
      size: {
        override: { classes: "p-0" },
      },
    },
  },
};

type SizeVariant = "xs" | "sm" | "md" | "lg" | "xl";
const SizeClasses: VariantConfigurationMap = {
  xs: { classes: "text-xs" },
  sm: { classes: "text-sm" },
  md: { classes: "text-md" },
  lg: { classes: "text-lg" },
  xl: { classes: "text-xl" },
};

export type ButtonProps = Omit<
  React.HTMLProps<HTMLButtonElement>,
  "size" | "label"
> & {
  type?: "button" | "submit" | "reset";
  size?: SizeVariant;
  theme?: ThemeVariant;
  rounded?: RoundedVariant;
  border?: BorderVariant;
  label?: string;
};

// TODO: support loading from config?
const DEFAULT_THEME: ThemeVariant = "primary";
const DEFAULT_ROUNDED: RoundedVariant = "rounded";
const DEFAULT_BORDER: BorderVariant = "none";
const DEFAULT_SIZE: SizeVariant = "md";

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      children,
      className,
      type = "button",
      size = DEFAULT_SIZE,
      theme = DEFAULT_THEME,
      rounded = DEFAULT_ROUNDED,
      border = DEFAULT_BORDER,
      label,
      ...rest
    },
    ref
  ) => {
    return (
      <button
        {...rest}
        className={classNames(
          // TODO: make variant system
          ThemeClasses[theme]?.classes,
          RoundedClasses[rounded]?.variants?.size?.override?.classes ||
            ThemeClasses[theme]?.variants?.size?.[size]?.classes,
          SizeClasses[size]?.classes,
          ThemeClasses[theme]?.variants?.border?.override?.classes ||
            BorderClasses[border]?.classes ||
            ThemeClasses[theme]?.variants?.border?.[border]?.classes,
          ThemeClasses[theme]?.variants?.rounded?.override?.classes ||
            RoundedClasses[rounded]?.classes ||
            ThemeClasses[theme]?.variants?.rounded?.[rounded]?.classes ||
            ThemeClasses[theme]?.variants?.rounded?.default?.classes,
          "font-medium disabled:opacity-60 cursor-pointer disabled:cursor-default",
          className
        )}
        type={type}
        ref={ref}
      >
        {label || children}
      </button>
    );
  }
);

Button.displayName = "Button";

export default Button;
