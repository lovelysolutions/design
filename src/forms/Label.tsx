import React, { forwardRef } from "react";

import isString from "lodash/isString";
import omit from "lodash/omit";
import classNames from "classnames";

type FieldLabelProps = React.HTMLProps<HTMLLabelElement> & {
  label?: string | JSX.Element;
  inputId?: string;
  labelClasses?: string;
};

const Label = forwardRef<HTMLLabelElement, FieldLabelProps>(
  ({ children, className, label, labelClasses, ...rest }, ref) => {
    return (
      <label
        className={classNames(className, "flex flex-col flex-grow")}
        {...omit(rest, "name", "value", "placeholder", "disabled")}
        ref={ref}
      >
        {isString(label) ? (
          <div
            className={classNames(
              "text-xs mb-1 h-4",
              labelClasses || "text-gray-700"
            )}
          >
            {label}
          </div>
        ) : (
          label
        )}
        {children}
      </label>
    );
  }
);

Label.displayName = "Label";

export default Label;
