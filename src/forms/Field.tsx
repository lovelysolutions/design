import classNames from "classnames";

import React, {
  FormEventHandler,
  forwardRef,
  MutableRefObject,
  useMemo,
} from "react";

import Input, { InputType } from "../inputs/Input";
import isNumber from "lodash/isNumber";
import Label from "./Label";
// import Upload from "../inputs/Upload";

export type FieldProps = Omit<
  React.HTMLProps<
    | HTMLDivElement
    | HTMLInputElement
    | HTMLTextAreaElement
    | HTMLSelectElement
    | HTMLButtonElement
    | HTMLLabelElement
  >,
  "value" | "ref" | "onChange" | "type"
> & {
  type?: InputType;
  label?: string;
  editing?: boolean;
  onChange?: (value: any) => void;
  value: any;
  hidden?: boolean;
};

const Field = forwardRef<
  | HTMLDivElement
  | HTMLInputElement
  | HTMLTextAreaElement
  | HTMLSelectElement
  | HTMLButtonElement
  | HTMLLabelElement,
  FieldProps
>(
  (
    {
      editing,
      label,
      value,
      className,
      onChange,
      hidden,
      placeholder,
      type = "text",
      name,
      id,
      ...rest
    },
    ref
  ) => {
    const fieldId = useMemo(
      () => id || `${name || type || "field"}-${String(new Date().getTime())}`,
      [id]
    );

    if (hidden) {
      return null;
    }

    if (editing) {
      const onChangeWrapper: FormEventHandler<
        | HTMLInputElement
        | HTMLTextAreaElement
        | HTMLSelectElement
        | HTMLButtonElement
        | HTMLLabelElement
      > = (event) => {
        const value = (event.target as HTMLInputElement | HTMLSelectElement)
          .value;

        onChange?.(value);
      };

      return (
        <div className={className}>
          <Label
            className={classNames("cursor-pointer relative")}
            label={label}
            // htmlFor={toggle ? undefined : fieldId}
            htmlFor={fieldId}
          >
            <Input
              {...rest}
              value={isNumber(value) ? value : value || ""}
              ref={ref as MutableRefObject<HTMLInputElement>}
              className={classNames(
                "border-b px-0.5 box-content flex-grow",
                type !== "textarea" && "h-7"
              )}
              onChange={onChangeWrapper}
              placeholder={placeholder || label}
              type={type}
              id={fieldId}
              name={name}
            />
          </Label>
        </div>
      );
    }

    return (
      <Label
        {...rest}
        className={className}
        label={label}
        ref={ref as MutableRefObject<HTMLLabelElement>}
      >
        <div
          className={classNames(
            "border-b border-transparent px-0.5 leading-7 flex-grow"
          )}
        >
          {value}
        </div>
      </Label>
    );
  }
);

Field.displayName = "Field";

export default Field;
