import { extend } from "colord";

import namesPlugin from "colord/plugins/names";

extend([namesPlugin]);

// console.log(colord("#a641e7").toName({ closest: true })); // darkorchid
// console.log(colord("#a0289d").toName({ closest: true })); // darkorchid

export default {};
