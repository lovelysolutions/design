import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Image from "../images/Image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

export default {
  title: "Atoms/Image",
  component: Image,
} as ComponentMeta<typeof Image>;

const Template: ComponentStory<typeof Image> = (args) => (
  <div className="w-20 h-20">
    <Image {...args} />
  </div>
);

export const Broken = Template.bind({});
Broken.args = {
  src: "broken",
};

export const IconBackup = Template.bind({});
IconBackup.args = {
  src: "broken",
  backup: <FontAwesomeIcon icon={faUser} size="3x" className="text-gray-500" />,
  className: "flex items-center justify-center",
};

export const InitialsBackup = Template.bind({});
const INITIALS = ["J", "C"];
InitialsBackup.args = {
  src: "broken",
  backup: (
    <div className="font-semibold text-2xl leading-10 text-gray-500 tracking-widest">
      {INITIALS.join("")}
    </div>
  ),
  className: "flex items-center justify-center",
};
