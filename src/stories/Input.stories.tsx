import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Input from "../inputs/Input";

export default {
  title: "Atoms/Input",
  component: Input,
  argTypes: {
    value: {
      description: "Input value",
    },
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => (
  <label className="flex cursor-pointer flex-grow">
    <Input {...args} />
  </label>
);

export const Text = Template.bind({});
Text.args = {
  placeholder: "Insert your text here",
};

export const Password = Template.bind({});
Password.args = {
  placeholder: "Enter a secure password here",
  type: "password",
  autoCompleteProps: {
    newPassword: true,
  },
};

export const Radio = Template.bind({});
Radio.args = {
  children: <span>Select this option</span>,
  type: "radio",
};

export const Checkbox = Template.bind({});
Checkbox.args = {
  children: <span>Check and uncheck this box</span>,
  type: "checkbox",
};
