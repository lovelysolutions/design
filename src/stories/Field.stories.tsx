import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Field from "../forms/Field";

export default {
  title: "Molecules/Field",
  component: Field,
  argTypes: {
    editing: {
      defaultValue: true,
      type: "boolean",
    },
    value: {
      description: "Field value",
    },
  },
} as ComponentMeta<typeof Field>;

const Template: ComponentStory<typeof Field> = (args) => <Field {...args} />;

export const FullName = Template.bind({});
FullName.args = {
  placeholder: "Insert your full name here",
  label: "Full name",
};

// "text"
// "select"
// "textarea"
// "number"
// "email"
// "password"
// "username"
// "url"
// "search"
// "range"
// "datetime-local"
// "date"
// "time"
// "week"
// "month"
// "color"
// "name"
// "fname"
// "mname"
// "lname"
// "address"
// "city"
// "region"
// "province"
// "state"
// "zip"
// "zip2"
// "postal"
// "country"
// "phone"
// "mobile"
// "country-code"
// "area-code"
// "exchange"
// "suffix"
// "ext"
// "ccname"
// "cardnumber"
// "cvc"
// "ccmonth"
// "ccyear"
// "exp-date"
// "card-type"
