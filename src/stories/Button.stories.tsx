import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Button from "../buttons/Button";

export default {
  title: "Atoms/Button",
  component: Button,
  argTypes: {
    label: {
      defaultValue: "Click me",
      description: "Text displayed within the button",
      type: "string",
    },
    title: {
      defaultValue: "Action to be taken",
      description: "Description of the action to be taken",
      type: "string",
    },
    children: {
      defaultValue: (
        <div className="w-10 h-10 leading-10 font-bold text-xl">+</div>
      ),
      description:
        "JSX can be passed into the button instead of the label property",
      type: "symbol",
    },
    onClick: {
      action: "clicked",
    },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Link = Template.bind({});
Link.args = {
  theme: "link",
};

export const Primary = Template.bind({});
Primary.args = {
  theme: "primary",
};

export const Secondary = Template.bind({});
Secondary.args = {
  theme: "secondary",
};

export const Light = Template.bind({});
Light.args = {
  theme: "light",
};

export const Dark = Template.bind({});
Dark.args = {
  theme: "dark",
};

export const Transparent = Template.bind({});
Transparent.args = {
  theme: "transparent",
};

export const Circle = Template.bind({});
Circle.args = {
  rounded: "circle",
  label: undefined,
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const XLarge = Template.bind({});
XLarge.args = {
  size: "xl",
};

export const Large = Template.bind({});
Large.args = {
  size: "lg",
};

export const Small = Template.bind({});
Small.args = {
  size: "sm",
};

export const XSmall = Template.bind({});
XSmall.args = {
  size: "xs",
};
