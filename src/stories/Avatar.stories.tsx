import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Avatar from "../images/Avatar";

export default {
  title: "Molecules/Avatar",
  component: Avatar,
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

export const Person = Template.bind({});
Person.args = {
  src: "https://images.unsplash.com/photo-1544725176-7c40e5a71c5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8",
};
