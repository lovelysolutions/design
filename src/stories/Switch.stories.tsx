import React, { useState } from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Switch from "../inputs/Switch";
import isBoolean from "lodash/isBoolean";

export default {
  title: "Atoms/Switch",
  component: Switch,
  argTypes: {
    value: {
      defaultValue: true,
      description: "Whether switch is toggled or not",
    },
  },
} as ComponentMeta<typeof Switch>;

const Template: ComponentStory<typeof Switch> = (args) => {
  const [value, setValue] = useState<boolean>(
    isBoolean(args.value) ? args.value : true
  );

  return (
    <Switch
      {...args}
      onChange={(updatedValue) => setValue(updatedValue)}
      value={value}
    />
  );
};

export const Default = Template.bind({});
