// Colors exported from LAB Natural theme https://components.ai/theme/siXqqCxejc9S5IPPtNVu?tab=export

/* eslint-disable @typescript-eslint/no-var-requires */
// const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      animation: {
        "fade-in": "fadeIn 0.5s ease-in-out",
      },
      keyframes: () => ({
        fadeIn: {
          "0%": { opacity: 0 },
          "100%": { opacity: "100%" },
        },
      }),
      colors: {
        primary: "#352c5b",
        "primary-lightened": "#412162",
        "primary-darkened": "#21174b",
        gray: {
          1: "#000000",
          2: "#1c1c1c",
          3: "#303030",
          4: "#474747",
          5: "#5d5d5d",
          6: "#757575",
          7: "#8c8c8c",
          8: "#a3a3a3",
          9: "#bababa",
          10: "#d1d1d1",
          11: "#e8e8e8",
          12: "#ffffff",
        },
        "blue-gray": {
          1: "#0e0e11",
          2: "#21222a",
          3: "#343544",
          4: "#484a5e",
          5: "#5c5f78",
          6: "#717490",
          7: "#8789a6",
          8: "#9c9eba",
          9: "#b1b3cb",
          10: "#c6c8db",
          11: "#dcdce9",
          12: "#f1f1f6",
        },
        blue: {
          1: "#0d0e1a",
          2: "#182142",
          3: "#1e336d",
          4: "#254797",
          5: "#325bbd",
          6: "#476fde",
          7: "#6284f6",
          8: "#809aff",
          9: "#9fb0ff",
          10: "#bcc6ff",
          11: "#d9deff",
          12: "#f4f5ff",
        },
        indigo: {
          1: "#120c1d",
          2: "#211b4d",
          3: "#2f297f",
          4: "#3e38b0",
          5: "#5049dd",
          6: "#675bff",
          7: "#8170ff",
          8: "#9d87ff",
          9: "#b7a0ff",
          10: "#d0baff",
          11: "#e5d6ff",
          12: "#f7f1ff",
        },
        violet: {
          1: "#170a1b",
          2: "#321545",
          3: "#501b71",
          4: "#6d239d",
          5: "#8a2fc5",
          6: "#a641e7",
          7: "#be58ff",
          8: "#d374ff",
          9: "#e392ff",
          10: "#f0b1ff",
          11: "#f9d0ff",
          12: "#fef0ff",
        },
        magenta: {
          1: "#170915",
          2: "#381436",
          3: "#5b1859",
          4: "#7f1e7c",
          5: "#a0289d", // Lovely?
          6: "#bf3abb",
          7: "#d853d2",
          8: "#ea70e4",
          9: "#f78ff0",
          10: "#ffaef8",
          11: "#ffcefc",
          12: "#ffeefe",
        },
        red: {
          1: "#19090a",
          2: "#3e131a",
          3: "#651829",
          4: "#8d1d38",
          5: "#b22749",
          6: "#d23a5b",
          7: "#ec5370",
          8: "#ff7086",
          9: "#ff909e",
          10: "#ffb0b9",
          11: "#ffd0d4",
          12: "#fff0f1",
        },
        orange: {
          1: "#200d02",
          2: "#431706",
          3: "#691e0a",
          4: "#8e280d",
          5: "#b13514",
          6: "#d14721",
          7: "#ea5e36",
          8: "#fd7950",
          9: "#ff9670",
          10: "#ffb495",
          11: "#ffd2be",
          12: "#fff0e9",
        },
        gold: {
          1: "#160f05",
          2: "#402e11",
          3: "#6e4d14",
          4: "#9c6c18",
          5: "#c68a20",
          6: "#eba62e",
          7: "#ffbe44",
          8: "#ffd15e",
          9: "#ffe07c",
          10: "#ffeb9b",
          11: "#fff3bc",
          12: "#fffade",
        },
        yellow: {
          1: "#0d0c04",
          2: "#332f11",
          3: "#585315",
          4: "#7f7719",
          5: "#a49a20",
          6: "#c5ba2c",
          7: "#e1d43f",
          8: "#f6e857",
          9: "#fff673",
          10: "#fffe90",
          11: "#ffffae",
          12: "#ffffcc",
        },
        lime: {
          1: "#161708",
          2: "#323711",
          3: "#505a15",
          4: "#6e7c19",
          5: "#8b9c22",
          6: "#a6b932",
          7: "#bed049",
          8: "#d1e264",
          9: "#e1ee83",
          10: "#edf6a3",
          11: "#f6fbc4",
          12: "#fcfee6",
        },
        green: {
          1: "#071209",
          2: "#102816",
          3: "#133f21",
          4: "#18572d",
          5: "#216f3b",
          6: "#32864b",
          7: "#499c60",
          8: "#66b178",
          9: "#85c492",
          10: "#a7d6af",
          11: "#cae7cf",
          12: "#eef7ef",
        },
        teal: {
          1: "#07110f",
          2: "#102722",
          3: "#133d35",
          4: "#18544a",
          5: "#216b5e",
          6: "#328173",
          7: "#499789",
          8: "#65ac9e",
          9: "#85c0b3",
          10: "#a7d3c9",
          11: "#cae5de",
          12: "#eef7f4",
        },
        cyan: {
          1: "#081111",
          2: "#112627",
          3: "#143c3e",
          4: "#185356",
          5: "#226a6d",
          6: "#338084",
          7: "#4b969a",
          8: "#67abae",
          9: "#87bfc1",
          10: "#a8d2d4",
          11: "#cbe4e5",
          12: "#eef6f7",
        },
        "green-gray": {
          1: "#0f1113",
          2: "#21262d",
          3: "#333c48",
          4: "#455364",
          5: "#59697f",
          6: "#6e8098",
          7: "#8396ae",
          8: "#99aac1",
          9: "#b0bed2",
          10: "#c6d1e1",
          11: "#dde4ed",
          12: "#f4f6f9",
        },
      },
    },
  },
  plugins: [],
};

// Coolors:

// /* CSV */
// f99202,c0494c,b2375f,860095,6b1478,50285b,412162,3b2063,813632,c64c00

// /* With # */
// #f99202, #c0494c, #b2375f, #860095, #6b1478, #50285b, #412162, #3b2063, #813632, #c64c00

// /* Array */
// ["f99202","c0494c","b2375f","860095","6b1478","50285b","412162","3b2063","813632","c64c00"]

// /* Object */
// {"Yellow Orange Color Wheel":"f99202","Bittersweet Shimmer":"c0494c","Maroon X 11":"b2375f","Dark Magenta":"860095","Patriarch":"6b1478","Palatinate Purple":"50285b","Russian Violet":"412162","Russian Violet 2":"3b2063","Brandy":"813632","Burnt Orange":"c64c00"}

// /* Extended Array */
// [{"name":"Yellow Orange Color Wheel","hex":"f99202","rgb":[249,146,2],"cmyk":[0,41,99,2],"hsb":[35,99,98],"hsl":[35,98,49],"lab":[70,31,75]},{"name":"Bittersweet Shimmer","hex":"c0494c","rgb":[192,73,76],"cmyk":[0,62,60,25],"hsb":[358,62,75],"hsl":[358,49,52],"lab":[48,48,24]},{"name":"Maroon X 11","hex":"b2375f","rgb":[178,55,95],"cmyk":[0,69,47,30],"hsb":[340,69,70],"hsl":[340,53,46],"lab":[43,53,5]},{"name":"Dark Magenta","hex":"860095","rgb":[134,0,149],"cmyk":[10,100,0,42],"hsb":[294,100,58],"hsl":[294,100,29],"lab":[32,63,-45]},{"name":"Patriarch","hex":"6b1478","rgb":[107,20,120],"cmyk":[11,83,0,53],"hsb":[292,83,47],"hsl":[292,71,27],"lab":[27,50,-37]},{"name":"Palatinate Purple","hex":"50285b","rgb":[80,40,91],"cmyk":[12,56,0,64],"hsb":[287,56,36],"hsl":[287,39,26],"lab":[24,28,-23]},{"name":"Russian Violet","hex":"412162","rgb":[65,33,98],"cmyk":[34,66,0,62],"hsb":[270,66,38],"hsl":[270,50,26],"lab":[20,29,-33]},{"name":"Russian Violet","hex":"3b2063","rgb":[59,32,99],"cmyk":[40,68,0,61],"hsb":[264,68,39],"hsl":[264,51,26],"lab":[19,29,-35]},{"name":"Brandy","hex":"813632","rgb":[129,54,50],"cmyk":[0,58,61,49],"hsb":[3,61,51],"hsl":[3,44,35],"lab":[33,32,19]},{"name":"Burnt Orange","hex":"c64c00","rgb":[198,76,0],"cmyk":[0,62,100,22],"hsb":[23,100,78],"hsl":[23,100,39],"lab":[48,46,59]}]

// /* XML */
// <palette>
//   <color name="Yellow Orange Color Wheel" hex="f99202" r="249" g="146" b="2" />
//   <color name="Bittersweet Shimmer" hex="c0494c" r="192" g="73" b="76" />
//   <color name="Maroon X 11" hex="b2375f" r="178" g="55" b="95" />
//   <color name="Dark Magenta" hex="860095" r="134" g="0" b="149" />
//   <color name="Patriarch" hex="6b1478" r="107" g="20" b="120" />
//   <color name="Palatinate Purple" hex="50285b" r="80" g="40" b="91" />
//   <color name="Russian Violet" hex="412162" r="65" g="33" b="98" />
//   <color name="Russian Violet" hex="3b2063" r="59" g="32" b="99" />
//   <color name="Brandy" hex="813632" r="129" g="54" b="50" />
//   <color name="Burnt Orange" hex="c64c00" r="198" g="76" b="0" />
// </palette>
